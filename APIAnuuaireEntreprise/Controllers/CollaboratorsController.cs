﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIAnuaireEntreprise.Models;
using APIAnnuaireEntreprise.Models;

namespace APIAnuaireEntreprise.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CollaboratorsController : ControllerBase
    {
        private readonly AnnuaireEntreprise _context;

        public CollaboratorsController(AnnuaireEntreprise context)
        {
            _context = context;
        }

        // GET: api/Collaborators
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Collaborator>>> GetCollaborators()
        {
            return await _context.Collaborators
                .Take(100)
                .ToListAsync();
        }

        // GET: api/Collaborators/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Collaborator>> GetCollaborator(int id)
        {
            var collaborator = await _context.Collaborators.FindAsync(id);

            if (collaborator == null)
            {
                return NotFound();
            }

            return collaborator;
        }

        // POST: api/Collaborators/Search
        [HttpPost("Search")]
        public async Task<ActionResult<IEnumerable<Collaborator>>> GetCollaboratorBySearch(SearchCollaborator searchCollaborator)
        {
            var collaborator = await _context.Collaborators.ToListAsync();

            if (searchCollaborator.firstname != "")
            {
                collaborator = collaborator
                    .Where(c => c.Firstname.Contains(searchCollaborator.firstname, StringComparison.CurrentCultureIgnoreCase))
                    .ToList();
            }
            if (searchCollaborator.lastname != "")
            {
                collaborator = collaborator
                    .Where(c => c.Lastname.Contains(searchCollaborator.lastname, StringComparison.CurrentCultureIgnoreCase))
                    .ToList();
            }
            if(searchCollaborator.idService != 0)
            {
                collaborator = collaborator
                    .Where(c => c.Service.Equals(searchCollaborator.idService))
                    .ToList();
            }
            if(searchCollaborator.idSite != 0)
            {
                collaborator = collaborator
                    .Where(c => c.Site.Equals(searchCollaborator.idSite))
                    .ToList();
            }

            collaborator = collaborator
                .Take(100)
                .ToList();


            if (collaborator == null)
            {
                return NotFound();
            }

            return collaborator;
        }

        // PUT: api/Collaborators/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCollaborator(int id, Collaborator collaborator)
        {
            if (id != collaborator.Id)
            {
                return BadRequest();
            }

            _context.Entry(collaborator).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CollaboratorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Collaborators
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Collaborator>> PostCollaborator(Collaborator collaborator)
        {
            _context.Collaborators.Add(collaborator);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCollaborator", new { id = collaborator.Id }, collaborator);
        }

        // DELETE: api/Collaborators/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCollaborator(int id)
        {
            var collaborator = await _context.Collaborators.FindAsync(id);
            if (collaborator == null)
            {
                return NotFound();
            }

            _context.Collaborators.Remove(collaborator);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CollaboratorExists(int id)
        {
            return _context.Collaborators.Any(e => e.Id == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace APIAnuaireEntreprise.Models;

public partial class Collaborator
{
    public int Id { get; set; }

    public string? Firstname { get; set; }

    public string? Lastname { get; set; }

    public string? Phone { get; set; }

    public string? Cellphone { get; set; }

    public string? Mail { get; set; }

    public int? Service { get; set; }

    public int? Site { get; set; }

    public DateTime? CreatedAt { get; set; }

    public DateTime? UpdatedAt { get; set; }

    public virtual Service? ServiceNavigation { get; set; }

    public virtual Site? SiteNavigation { get; set; }
}

﻿namespace APIAnnuaireEntreprise.Models
{
    public partial class SearchCollaborator
    {
        public string firstname { get; set; } = "";
        public string lastname { get; set; } = "";
        public int idService { get; set; } = 0;
        public int idSite { get; set; } = 0;
    }
}

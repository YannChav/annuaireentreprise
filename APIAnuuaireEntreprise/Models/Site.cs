﻿using System;
using System.Collections.Generic;

namespace APIAnuaireEntreprise.Models;

public partial class Site
{
    public int Id { get; set; }

    public string? City { get; set; }

    public string? Adress { get; set; }

    public string? Postcode { get; set; }

    public string Role { get; set; } = null!;

    public DateTime? CreatedAt { get; set; }

    public DateTime? UpdatedAt { get; set; }

    public virtual ICollection<Collaborator> Collaborators { get; } = new List<Collaborator>();
}

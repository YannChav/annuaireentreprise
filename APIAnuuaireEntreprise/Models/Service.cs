﻿using System;
using System.Collections.Generic;

namespace APIAnuaireEntreprise.Models;

public partial class Service
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public DateTime? CreatedAt { get; set; }

    public DateTime? UpdatedAt { get; set; }

    public virtual ICollection<Collaborator> Collaborators { get; } = new List<Collaborator>();
}

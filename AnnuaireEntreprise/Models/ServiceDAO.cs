﻿using AnnuaireEntreprise.Controllers;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;

namespace AnnuaireEntreprise.Models;

public class ServiceDAO
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public DateTime? CreatedAt { get; set; }

    public DateTime? UpdatedAt { get; set; }

    public bool DeleteService()
    {
        // Met en place l'URI pour la requête
        String uri = "Services/" + this.Id;
        // Crée un objet HTTPRequest et exécute la requête
        HTTPRequest httpRequest = new HTTPRequest(uri);
        httpRequest.ExecuteQueryDELETE();

        return httpRequest.State;
    }

    public bool AddService()
    {
        // Met en place l'URI pour la requête
        String uri = "Services";
        // Met en place le JSON pour créer l'objet
        string jsonString = JsonConvert.SerializeObject(this);

        // Crée un objet HTTPRequest et exécute la requête
        HTTPRequest httpRequest = new HTTPRequest(uri);
        httpRequest.ExecuteQueryPOST(jsonString);

        return httpRequest.State;
    }

    public bool UpdateService()
    {
        // Met en place l'URI pour la requête
        String uri = "Services/" + this.Id;
        // Met en place le JSON pour créer l'objet
        string jsonString = JsonConvert.SerializeObject(this);

        // Crée un objet HTTPRequest et exécute la requête
        HTTPRequest httpRequest = new HTTPRequest(uri);
        httpRequest.ExecuteQueryPUT(jsonString);

        return httpRequest.State;
    }

    public static ArrayList GetServices()
    {
        // Met en place l'URI pour la requête
        String uri = "Services";
        // Crée un objet HTTPRequest et exécute la requête
        HTTPRequest httpRequest = new HTTPRequest(uri);
        httpRequest.ExecuteQueryGET();

        // Créé un tableau de retour
        ArrayList tabResult = new ArrayList();

        if (httpRequest.State)
        {
            // Récupère tous les services
            var listObjects = JsonConvert.DeserializeObject<List<ServiceDAO>>(httpRequest.DataResponse);
            // Ajoute tous les services à la liste
            foreach (ServiceDAO service in listObjects)
            {
                tabResult.Add(service);
            }
        }

        return tabResult;
    }

    public static (bool state, ServiceDAO service) GetService(int id)
    {
        // Met en place l'URI pour la requête
        String uri = "Services/" + id;

        // Crée un objet HTTPRequest et exécute la requête
        HTTPRequest httpRequest = new HTTPRequest(uri);
        httpRequest.ExecuteQueryGET();

        // Vérifie l'état d'exécution de la requête
        if (httpRequest.State)
        {
            return (httpRequest.State, JsonConvert.DeserializeObject<ServiceDAO>(httpRequest.DataResponse));
        }
        else
        {
            return (httpRequest.State, new ServiceDAO());
        }
    }
}

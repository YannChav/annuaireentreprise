﻿using AnnuaireEntreprise.Controllers;
using Newtonsoft.Json;
using System.Collections;

namespace AnnuaireEntreprise.Models;

public class SiteDAO
{
    public int Id { get; set; }

    public string? City { get; set; }

    public string? Adress { get; set; }

    public string? Postcode { get; set; }

    public string Role { get; set; } = null!;

    public DateTime? CreatedAt { get; set; }

    public DateTime? UpdatedAt { get; set; }

    public bool DeleteSite()
    {
        // Met en place l'URI pour la requête
        String uri = "Sites/" + this.Id;
        // Crée un objet HTTPRequest et exécute la requête
        HTTPRequest httpRequest = new HTTPRequest(uri);
        httpRequest.ExecuteQueryDELETE();

        return httpRequest.State;
    }

    public bool AddSite()
    {
        // Met en place l'URI pour la requête
        String uri = "Sites";
        // Met en place le JSON pour créer l'objet
        string jsonString = JsonConvert.SerializeObject(this);

        // Crée un objet HTTPRequest et exécute la requête
        HTTPRequest httpRequest = new HTTPRequest(uri);
        httpRequest.ExecuteQueryPOST(jsonString);

        return httpRequest.State;
    }

    public bool UpdateSite()
    {
        // Met en place l'URI pour la requête
        String uri = "Sites/" + this.Id;
        // Met en place le JSON pour créer l'objet
        string jsonString = JsonConvert.SerializeObject(this);

        // Crée un objet HTTPRequest et exécute la requête
        HTTPRequest httpRequest = new HTTPRequest(uri);
        httpRequest.ExecuteQueryPUT(jsonString);

        return httpRequest.State;
    }

    public static ArrayList GetSites()
    {
        // Met en place l'URI pour la requête
        String uri = "Sites";
        // Crée un objet HTTPRequest et exécute la requête
        HTTPRequest httpRequest = new HTTPRequest(uri);
        httpRequest.ExecuteQueryGET();

        // Créé un tableau de retour
        ArrayList tabResult = new ArrayList();

        if (httpRequest.State)
        {
            // Récupère tous les sites
            var listObjects = JsonConvert.DeserializeObject<List<SiteDAO>>(httpRequest.DataResponse);
            // Ajoute tous les sites à la liste
            foreach (SiteDAO site in listObjects)
            {
                tabResult.Add(site);
            }
        }

        return tabResult;
    }

    public static (bool state, SiteDAO site) GetSite(int id)
    {
        // Met en place l'URI pour la requête
        String uri = "Sites/" + id;

        // Crée un objet HTTPRequest et exécute la requête
        HTTPRequest httpRequest = new HTTPRequest(uri);
        httpRequest.ExecuteQueryGET();

        // Vérifie l'état d'exécution de la requête
        if (httpRequest.State)
        {
            return (httpRequest.State, JsonConvert.DeserializeObject<SiteDAO>(httpRequest.DataResponse));
        }
        else
        {
            return (httpRequest.State, new SiteDAO());
        }
    }
}

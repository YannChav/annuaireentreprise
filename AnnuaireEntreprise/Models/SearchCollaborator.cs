﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnnuaireEntreprise.Models
{
    public class SearchCollaborator
    {
        public string firstname { get; set; } = "";
        public string lastname { get; set; } = "";
        public int idService { get; set; } = 0;
        public int idSite { get; set; } = 0;
    }
}

﻿using AnnuaireEntreprise.Controllers;
using AnnuaireEntreprise.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;

namespace AnnuaireEntreprise.Models;

public partial class CollaboratorDAO
{
    public int Id { get; set; }

    public string? Firstname { get; set; }

    public string? Lastname { get; set; }

    public string? Phone { get; set; }

    public string? Cellphone { get; set; }

    public string? Mail { get; set; }

    public int? Service { get; set; }

    public int? Site { get; set; }

    public DateTime? CreatedAt { get; set; }

    public DateTime? UpdatedAt { get; set; }

    public bool DeleteCollaborator()
    {
        // Met en place l'URI pour la requête
        String uri = "Collaborators/" + this.Id;
        // Crée un objet HTTPRequest et exécute la requête
        HTTPRequest httpRequest = new HTTPRequest(uri);
        httpRequest.ExecuteQueryDELETE();

        return httpRequest.State;
    }

    public bool AddCollaborator()
    {
        // Met en place l'URI pour la requête
        String uri = "Collaborators";
        // Met en place le JSON pour créer l'objet
        string jsonString = JsonConvert.SerializeObject(this);

        // Crée un objet HTTPRequest et exécute la requête
        HTTPRequest httpRequest = new HTTPRequest(uri);
        httpRequest.ExecuteQueryPOST(jsonString);

        return httpRequest.State;
    }

    public bool UpdateCollaborator()
    {
        // Met en place l'URI pour la requête
        String uri = "Collaborators/" + this.Id;
        // Met en place le JSON pour créer l'objet
        string jsonString = JsonConvert.SerializeObject(this);

        // Crée un objet HTTPRequest et exécute la requête
        HTTPRequest httpRequest = new HTTPRequest(uri);
        httpRequest.ExecuteQueryPUT(jsonString);

        return httpRequest.State;
    }

    public static ArrayList GetCollaborators(SearchCollaborator collaboratorParameters)
    {
        // Met en place l'URI pour la requête
        String uri = "Collaborators/Search";
        // Met en place le JSON pour créer l'objet
        string jsonString = JsonConvert.SerializeObject(collaboratorParameters);

        // Crée un objet HTTPRequest et exécute la requête
        HTTPRequest httpRequest = new HTTPRequest(uri);
        httpRequest.ExecuteQueryPOST(jsonString);

        // Créé un tableau de retour
        ArrayList tabResult = new ArrayList();

        if (httpRequest.State)
        {
            // Récupère tous les sites
            var listObjects = JsonConvert.DeserializeObject<List<CollaboratorDAO>>(httpRequest.DataResponse);
            // Ajoute tous les sites à la liste
            foreach (CollaboratorDAO collaborator in listObjects)
            {
                tabResult.Add(collaborator);
            }
        }

        return tabResult;
    }

    public static (bool state, CollaboratorDAO collaborator) GetCollaborator(int id)
    {
        // Met en place l'URI pour la requête
        String uri = "Collaborators/" + id;

        // Crée un objet HTTPRequest et exécute la requête
        HTTPRequest httpRequest = new HTTPRequest(uri);
        httpRequest.ExecuteQueryGET();

        // Vérifie l'état d'exécution de la requête
        if (httpRequest.State)
        {
            return (httpRequest.State, JsonConvert.DeserializeObject<CollaboratorDAO>(httpRequest.DataResponse));
        }
        else
        {
            return (httpRequest.State, new CollaboratorDAO());
        }
    }
}

﻿namespace AnnuaireEntreprise
{
    partial class WIN_SERVICE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WIN_SERVICE));
            this.LBL_SERVICE = new System.Windows.Forms.Label();
            this.CMB_SERVICE = new System.Windows.Forms.ComboBox();
            this.GB_CONSULT_SERVICE = new System.Windows.Forms.GroupBox();
            this.INP_CRETAED_AT = new System.Windows.Forms.TextBox();
            this.LBL_CREATED_AT = new System.Windows.Forms.Label();
            this.INP_UPDATED_AT = new System.Windows.Forms.TextBox();
            this.LBL_UPDATED_AT = new System.Windows.Forms.Label();
            this.INP_ID = new System.Windows.Forms.TextBox();
            this.LBL_ID = new System.Windows.Forms.Label();
            this.BTN_SAVE = new System.Windows.Forms.Button();
            this.BTN_DELETE = new System.Windows.Forms.Button();
            this.INP_CONSULT_NAME = new System.Windows.Forms.TextBox();
            this.GB_CREATE_SERVICE = new System.Windows.Forms.GroupBox();
            this.BTN_CREATE = new System.Windows.Forms.Button();
            this.INP_CREATE_NAME = new System.Windows.Forms.TextBox();
            this.LBL_CREATE_NAME = new System.Windows.Forms.Label();
            this.BTN_FINISH = new System.Windows.Forms.Button();
            this.GB_CONSULT_SERVICE.SuspendLayout();
            this.GB_CREATE_SERVICE.SuspendLayout();
            this.SuspendLayout();
            // 
            // LBL_SERVICE
            // 
            this.LBL_SERVICE.AutoSize = true;
            this.LBL_SERVICE.Location = new System.Drawing.Point(122, 85);
            this.LBL_SERVICE.Name = "LBL_SERVICE";
            this.LBL_SERVICE.Size = new System.Drawing.Size(57, 23);
            this.LBL_SERVICE.TabIndex = 0;
            this.LBL_SERVICE.Text = "Nom :";
            // 
            // CMB_SERVICE
            // 
            this.CMB_SERVICE.FormattingEnabled = true;
            this.CMB_SERVICE.Location = new System.Drawing.Point(67, 29);
            this.CMB_SERVICE.Name = "CMB_SERVICE";
            this.CMB_SERVICE.Size = new System.Drawing.Size(307, 31);
            this.CMB_SERVICE.TabIndex = 2;
            this.CMB_SERVICE.SelectedIndexChanged += new System.EventHandler(this.CMB_SERVICE_SelectedIndexChanged);
            // 
            // GB_CONSULT_SERVICE
            // 
            this.GB_CONSULT_SERVICE.Controls.Add(this.INP_CRETAED_AT);
            this.GB_CONSULT_SERVICE.Controls.Add(this.LBL_CREATED_AT);
            this.GB_CONSULT_SERVICE.Controls.Add(this.INP_UPDATED_AT);
            this.GB_CONSULT_SERVICE.Controls.Add(this.LBL_UPDATED_AT);
            this.GB_CONSULT_SERVICE.Controls.Add(this.INP_ID);
            this.GB_CONSULT_SERVICE.Controls.Add(this.LBL_ID);
            this.GB_CONSULT_SERVICE.Controls.Add(this.BTN_SAVE);
            this.GB_CONSULT_SERVICE.Controls.Add(this.BTN_DELETE);
            this.GB_CONSULT_SERVICE.Controls.Add(this.INP_CONSULT_NAME);
            this.GB_CONSULT_SERVICE.Controls.Add(this.CMB_SERVICE);
            this.GB_CONSULT_SERVICE.Controls.Add(this.LBL_SERVICE);
            this.GB_CONSULT_SERVICE.Location = new System.Drawing.Point(12, 12);
            this.GB_CONSULT_SERVICE.Name = "GB_CONSULT_SERVICE";
            this.GB_CONSULT_SERVICE.Size = new System.Drawing.Size(441, 218);
            this.GB_CONSULT_SERVICE.TabIndex = 3;
            this.GB_CONSULT_SERVICE.TabStop = false;
            this.GB_CONSULT_SERVICE.Text = "Consultation";
            // 
            // INP_CRETAED_AT
            // 
            this.INP_CRETAED_AT.Enabled = false;
            this.INP_CRETAED_AT.Location = new System.Drawing.Point(93, 118);
            this.INP_CRETAED_AT.Name = "INP_CRETAED_AT";
            this.INP_CRETAED_AT.Size = new System.Drawing.Size(95, 30);
            this.INP_CRETAED_AT.TabIndex = 12;
            // 
            // LBL_CREATED_AT
            // 
            this.LBL_CREATED_AT.AutoSize = true;
            this.LBL_CREATED_AT.Location = new System.Drawing.Point(15, 121);
            this.LBL_CREATED_AT.Name = "LBL_CREATED_AT";
            this.LBL_CREATED_AT.Size = new System.Drawing.Size(72, 23);
            this.LBL_CREATED_AT.TabIndex = 11;
            this.LBL_CREATED_AT.Text = "Créé le :";
            // 
            // INP_UPDATED_AT
            // 
            this.INP_UPDATED_AT.Enabled = false;
            this.INP_UPDATED_AT.Location = new System.Drawing.Point(330, 118);
            this.INP_UPDATED_AT.Name = "INP_UPDATED_AT";
            this.INP_UPDATED_AT.Size = new System.Drawing.Size(95, 30);
            this.INP_UPDATED_AT.TabIndex = 10;
            // 
            // LBL_UPDATED_AT
            // 
            this.LBL_UPDATED_AT.AutoSize = true;
            this.LBL_UPDATED_AT.Location = new System.Drawing.Point(231, 121);
            this.LBL_UPDATED_AT.Name = "LBL_UPDATED_AT";
            this.LBL_UPDATED_AT.Size = new System.Drawing.Size(94, 23);
            this.LBL_UPDATED_AT.TabIndex = 9;
            this.LBL_UPDATED_AT.Text = "Modifié le :";
            // 
            // INP_ID
            // 
            this.INP_ID.Enabled = false;
            this.INP_ID.Location = new System.Drawing.Point(48, 81);
            this.INP_ID.Name = "INP_ID";
            this.INP_ID.Size = new System.Drawing.Size(68, 30);
            this.INP_ID.TabIndex = 8;
            // 
            // LBL_ID
            // 
            this.LBL_ID.AutoSize = true;
            this.LBL_ID.Location = new System.Drawing.Point(6, 85);
            this.LBL_ID.Name = "LBL_ID";
            this.LBL_ID.Size = new System.Drawing.Size(36, 23);
            this.LBL_ID.TabIndex = 7;
            this.LBL_ID.Text = "ID :";
            // 
            // BTN_SAVE
            // 
            this.BTN_SAVE.Location = new System.Drawing.Point(239, 168);
            this.BTN_SAVE.Name = "BTN_SAVE";
            this.BTN_SAVE.Size = new System.Drawing.Size(112, 35);
            this.BTN_SAVE.TabIndex = 6;
            this.BTN_SAVE.Text = "Enregistrer";
            this.BTN_SAVE.UseVisualStyleBackColor = true;
            this.BTN_SAVE.Click += new System.EventHandler(this.BTN_SAVE_Click);
            // 
            // BTN_DELETE
            // 
            this.BTN_DELETE.Location = new System.Drawing.Point(90, 168);
            this.BTN_DELETE.Name = "BTN_DELETE";
            this.BTN_DELETE.Size = new System.Drawing.Size(112, 35);
            this.BTN_DELETE.TabIndex = 5;
            this.BTN_DELETE.Text = "Supprimer";
            this.BTN_DELETE.UseVisualStyleBackColor = true;
            this.BTN_DELETE.Click += new System.EventHandler(this.BTN_DELETE_Click);
            // 
            // INP_CONSULT_NAME
            // 
            this.INP_CONSULT_NAME.Location = new System.Drawing.Point(185, 81);
            this.INP_CONSULT_NAME.Name = "INP_CONSULT_NAME";
            this.INP_CONSULT_NAME.Size = new System.Drawing.Size(250, 30);
            this.INP_CONSULT_NAME.TabIndex = 4;
            // 
            // GB_CREATE_SERVICE
            // 
            this.GB_CREATE_SERVICE.Controls.Add(this.BTN_CREATE);
            this.GB_CREATE_SERVICE.Controls.Add(this.INP_CREATE_NAME);
            this.GB_CREATE_SERVICE.Controls.Add(this.LBL_CREATE_NAME);
            this.GB_CREATE_SERVICE.Location = new System.Drawing.Point(459, 12);
            this.GB_CREATE_SERVICE.Name = "GB_CREATE_SERVICE";
            this.GB_CREATE_SERVICE.Size = new System.Drawing.Size(331, 138);
            this.GB_CREATE_SERVICE.TabIndex = 4;
            this.GB_CREATE_SERVICE.TabStop = false;
            this.GB_CREATE_SERVICE.Text = "Création";
            // 
            // BTN_CREATE
            // 
            this.BTN_CREATE.Location = new System.Drawing.Point(109, 86);
            this.BTN_CREATE.Name = "BTN_CREATE";
            this.BTN_CREATE.Size = new System.Drawing.Size(112, 35);
            this.BTN_CREATE.TabIndex = 7;
            this.BTN_CREATE.Text = "Créer";
            this.BTN_CREATE.UseVisualStyleBackColor = true;
            this.BTN_CREATE.Click += new System.EventHandler(this.BTN_CREATE_Click);
            // 
            // INP_CREATE_NAME
            // 
            this.INP_CREATE_NAME.Location = new System.Drawing.Point(69, 39);
            this.INP_CREATE_NAME.Name = "INP_CREATE_NAME";
            this.INP_CREATE_NAME.Size = new System.Drawing.Size(250, 30);
            this.INP_CREATE_NAME.TabIndex = 5;
            // 
            // LBL_CREATE_NAME
            // 
            this.LBL_CREATE_NAME.AutoSize = true;
            this.LBL_CREATE_NAME.Location = new System.Drawing.Point(6, 39);
            this.LBL_CREATE_NAME.Name = "LBL_CREATE_NAME";
            this.LBL_CREATE_NAME.Size = new System.Drawing.Size(57, 23);
            this.LBL_CREATE_NAME.TabIndex = 1;
            this.LBL_CREATE_NAME.Text = "Nom :";
            // 
            // BTN_FINISH
            // 
            this.BTN_FINISH.Location = new System.Drawing.Point(678, 194);
            this.BTN_FINISH.Name = "BTN_FINISH";
            this.BTN_FINISH.Size = new System.Drawing.Size(112, 35);
            this.BTN_FINISH.TabIndex = 6;
            this.BTN_FINISH.Text = "Quitter";
            this.BTN_FINISH.UseVisualStyleBackColor = true;
            this.BTN_FINISH.Click += new System.EventHandler(this.BTN_FINISH_Click);
            // 
            // WIN_SERVICE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(803, 242);
            this.Controls.Add(this.BTN_FINISH);
            this.Controls.Add(this.GB_CREATE_SERVICE);
            this.Controls.Add(this.GB_CONSULT_SERVICE);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(821, 289);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(821, 289);
            this.Name = "WIN_SERVICE";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Text = "Service";
            this.Load += new System.EventHandler(this.WIN_SERVICE_Load);
            this.GB_CONSULT_SERVICE.ResumeLayout(false);
            this.GB_CONSULT_SERVICE.PerformLayout();
            this.GB_CREATE_SERVICE.ResumeLayout(false);
            this.GB_CREATE_SERVICE.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Label LBL_SERVICE;
        private ComboBox CMB_SERVICE;
        private GroupBox GB_CONSULT_SERVICE;
        private TextBox INP_CONSULT_NAME;
        private Button BTN_SAVE;
        private Button BTN_DELETE;
        private GroupBox GB_CREATE_SERVICE;
        private TextBox INP_CREATE_NAME;
        private Label LBL_CREATE_NAME;
        private Button BTN_FINISH;
        private Button BTN_CREATE;
        private Label LBL_ID;
        private TextBox INP_CRETAED_AT;
        private Label LBL_CREATED_AT;
        private TextBox INP_UPDATED_AT;
        private Label LBL_UPDATED_AT;
        private TextBox INP_ID;
    }
}
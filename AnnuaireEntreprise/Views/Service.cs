﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AnnuaireEntreprise.Models;
using Microsoft.Toolkit.Uwp.Notifications;

namespace AnnuaireEntreprise
{
    public partial class WIN_SERVICE : Form
    {
        public WIN_SERVICE()
        {
            InitializeComponent();
        }

        public void Reload()
        {
            // Vide la liste des services
            CMB_SERVICE.Items.Clear();
            CMB_SERVICE.Text = "";
            // Récupère l'ensemble des services disponibles
            ArrayList tabServices = new ArrayList();
            tabServices = ServiceDAO.GetServices();

            // Parcourt tous les services récupérés
            foreach (ServiceDAO service in tabServices)
            {
                // Ajoute le service à la liste déroulante
                CMB_SERVICE.Items.Add(service.Name + " | " + service.Id);
            }
            // Vide les champs
            INP_ID.Text = "";
            INP_CONSULT_NAME.Text = "";
            INP_CREATE_NAME.Text = "";
            INP_CRETAED_AT.Text = "";
            INP_UPDATED_AT.Text = "";
        }

        private void WIN_SERVICE_Load(object sender, EventArgs e)
        {
            // Appelle la fonction pour charger la page
            this.Reload();
        }

        private void BTN_FINISH_Click(object sender, EventArgs e)
        {
            // Quitte la fenêtre
            this.Close();
        }

        private void CMB_SERVICE_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Récupère l'id du service selectionné
            int idService = int.Parse((CMB_SERVICE.Text.ToString().Split(" | "))[1]);

            // Crée un objet Service
            (bool state, ServiceDAO service) resultGetService = ServiceDAO.GetService(idService);

            // Récupère les informations de l'objet Service
            if (resultGetService.state)
            {
                ServiceDAO service = resultGetService.service;
                // Affiche les informations du service dans la fenêtre
                INP_ID.Text = service.Id.ToString();
                INP_CONSULT_NAME.Text = service.Name;
                INP_CRETAED_AT.Text = service.CreatedAt.ToString();
                INP_UPDATED_AT.Text = service.UpdatedAt.ToString();
            }
            else
            {
                // Affiche un message d'erreur à l'utilisateur
                MessageBox.Show("Les données du service sélectionné n'ont pas pu être récupérées.");
                // Recharge les données de la fenêtre
                this.Reload();
            }
        }

        private void BTN_DELETE_Click(object sender, EventArgs e)
        {
            // Vérifie qu'un service est selectionné
            if (CMB_SERVICE.Text.ToString() != "")
            {
                // Récupère l'id du service selectionné
                int idService = int.Parse((CMB_SERVICE.Text.ToString().Split(" | "))[1]);

                // Crée un objet Service
                ServiceDAO service = ServiceDAO.GetService(idService).service;

                // Met en place les paramètres de la requête
                SearchCollaborator collaboratorParameters = new SearchCollaborator();
                collaboratorParameters.idService = idService;

                // Récupère les informations de l'objet Service
                if (CollaboratorDAO.GetCollaborators(collaboratorParameters).Count > 0)
                {
                    // Affiche un message d'erreur à l'utilisateur
                    MessageBox.Show("Ce service ne peut pas être supprimé car des collaborateurs " +
                        "y travaillent.");
                }
                else
                {
                    // Vérifie si la suppression du service à bien eue lieu
                    if (!service.DeleteService())
                    {
                        // Affiche un message d'erreur à l'utilisateur
                        MessageBox.Show("Une erreur s'est produite lors de la suppression du service.");
                    }
                    else
                    {
                        // Affiche un toast à l'utilisateur
                        new ToastContentBuilder()
                           .AddText("La suppression du service s'est correctement effectuée.")
                           .SetToastDuration(ToastDuration.Short)
                           .Show();
                    }
                }
            }
            else
            {
                // Affiche un message à l'utilisateur
                MessageBox.Show("Veuillez sélectionner un service pour le supprimer");
            }

            // Rafraîchit les données de la fenêtre
            this.Reload();
        }

        private void BTN_SAVE_Click(object sender, EventArgs e)
        {
            // Enregistre les modifications du service si le nom n'est pas vide
            if (INP_CONSULT_NAME.Text != "")
            {
                // Créé un objet service et le met à jour
                ServiceDAO service = ServiceDAO.GetService(int.Parse(INP_ID.Text)).service;
                service.Name = INP_CONSULT_NAME.Text;
                service.UpdatedAt = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);

                if (!service.UpdateService())
                {
                    // Affiche un message d'erreur à l'utilisateur
                    MessageBox.Show("Une erreur s'est produite lors de la mise à jour du service.");
                }
                else
                {
                    // Affiche un toast à l'utilisateur
                    new ToastContentBuilder()
                       .AddText("La mise à jour du service s'est correctement effectuée.")
                       .SetToastDuration(ToastDuration.Short)
                       .Show();
                }

                // Rafraîchit les données de la fenêtre
                this.Reload();
            }
            else
            {
                // Affiche un message d'erreur à l'utilisateur
                MessageBox.Show("Veuillez renseigner un nom pour modifier un service.");
            }
        }

        private void BTN_CREATE_Click(object sender, EventArgs e)
        {
            // Vérifie que le nom est renseginé
            if (INP_CREATE_NAME.Text != "")
            {
                // Créé un objet Service et l'enregistre en BDD
                ServiceDAO service = new ServiceDAO();
                service.Name = INP_CREATE_NAME.Text;
                service.CreatedAt = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
                service.UpdatedAt = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);

                if (!service.AddService())
                {
                    // Affiche un message d'erreur à l'utilisateur
                    MessageBox.Show("Une erreur s'est produite lors de la création du service.");
                }
                else
                {
                    // Affiche un toast à l'utilisateur
                    new ToastContentBuilder()
                       .AddText("La création du service s'est correctement effectuée.")
                       .SetToastDuration(ToastDuration.Short)
                       .Show();
                }

                // Rafraîchit les données de la fenêtre
                this.Reload();
            }
            else
            {
                // Affiche un message d'erreur à l'utilisateur
                MessageBox.Show("Veuillez renseigner un nom pour créer un service.");
            }
        }
    }
}

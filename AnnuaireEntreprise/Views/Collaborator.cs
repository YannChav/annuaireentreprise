﻿using AnnuaireEntreprise.Controllers;
using AnnuaireEntreprise.Models;
using Microsoft.Toolkit.Uwp.Notifications;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnnuaireEntreprise
{
    public partial class WIN_COLLABORATOR : Form
    {
        public int idCollaborator;
        // stateWindow : L'état de lancement de la fenêtre
        //  + 1 : Consultation d'information
        //  + 2 : Modification / Suppression utilisateur
        //  + 3 : Ajout utilisateur
        public int stateWindow;
        public WIN_COLLABORATOR(int IdCollaborator, int StateWindow = 1)
        {
            InitializeComponent();
            this.idCollaborator = IdCollaborator;
            this.stateWindow = StateWindow;
        }  

        private void WIN_COLLABORATOR_Load(object sender, EventArgs e)
        {
            // Récupère l'ensemble des sites et des services disponibles
            ArrayList tabSites = new ArrayList();
            tabSites = SiteDAO.GetSites();

            ArrayList tabServices = new ArrayList();
            tabServices = ServiceDAO.GetServices();

            // Affiche, grise ou masque des champs en focntion du style de lancement de la fenêtre
            if (this.stateWindow == 3)
            { 
                // BOUTTONS
                BTN_DELETE.Visible = false;
                BTN_UPDATE.Visible = false;
                BTN_CANCEL.Text = "Enregistrer";

                // LABELS et CHAMPS
                this.Text = "Collaborateurs - Administrateur";
                LBL_COLLABORATOR.Text = "Ajout collaborateur";
                // Parcourt tous les services récupérés
                foreach (ServiceDAO service in tabServices)
                {
                    // Ajoute le service à la liste déroulante
                    CMB_SERVICE.Items.Add(service.Name + " | " + service.Id);
                }
                // Parcourt tous les sites récupérés
                foreach (SiteDAO site in tabSites)
                {
                    // Ajoute le site à la liste déroulante
                    CMB_SITE.Items.Add(site.City + " | " + site.Id);
                }
            }
            else
            {
                // Récupère l'utilsiateur passé en paramètres
                CollaboratorDAO collaborator = CollaboratorDAO.GetCollaborator(this.idCollaborator).collaborator;

                // Complète les champs de saisi avec le collaborateur récupéré
                LBL_COLLABORATOR.Text = $"Fiche collaborateur  {collaborator.Firstname} {collaborator.Lastname}";

                INP_FIRSTNAME.Text = collaborator.Firstname;
                INP_LASTNAME.Text = collaborator.Lastname;
                INP_PHONE.Text = collaborator.Phone;
                INP_CELLPHONE.Text = collaborator.Cellphone;
                INP_MAIL.Text = collaborator.Mail;

                // Parcourt tous les services récupérés
                foreach (ServiceDAO service in tabServices)
                {
                    // Ajoute le service à la liste déroulante
                    CMB_SERVICE.Items.Add(service.Name + " | " + service.Id);
                    // Si l'id du service correspond à celui du collaborateur
                    CMB_SERVICE.Text = collaborator.Service == service.Id ? service.Name + " | " + service.Id : CMB_SERVICE.Text;
                }
                // Parcourt tous les sites récupérés
                foreach (SiteDAO site in tabSites)
                {
                    // Ajoute le site à la liste déroulante
                    CMB_SITE.Items.Add(site.City + " | " + site.Id);
                    // Si l'id du service correspond à celui du collaborateur
                    CMB_SITE.Text = collaborator.Site == site.Id ? site.City + " | " + site.Id : CMB_SITE.Text;
                }

                if (this.stateWindow == 2)
                {
                    this.Text = "Collaborateurs - Administrateur";
                    BTN_CANCEL.Visible = false;
                }
                else if (this.stateWindow == 1)
                {
                    // BOUTTONS
                    BTN_DELETE.Visible = false;
                    BTN_UPDATE.Visible = false;

                    // CHAMPS
                    INP_FIRSTNAME.Enabled = false;
                    INP_LASTNAME.Enabled = false;
                    INP_PHONE.Enabled = false;
                    INP_CELLPHONE.Enabled = false;
                    INP_MAIL.Enabled = false;
                    CMB_SERVICE.Enabled = false;
                    CMB_SITE.Enabled = false;
                }
            }
        }

        private void BTN_CANCEL_Click(object sender, EventArgs e)
        {
            // Vérifie si l'on est dans l'état pour créer un collaborateur
            if (this.stateWindow == 3)
            {
                // Vérifie que les champs soient complétés
                if (INP_FIRSTNAME.Text == "")
                {
                    MessageBox.Show("Veuillez rentrer le prénom du collaborateur à créer.");
                    INP_FIRSTNAME.Focus();
                }
                else if(INP_LASTNAME.Text == "")
                {
                    MessageBox.Show("Veuillez rentrer le nom du collaborateur à créer.");
                    INP_LASTNAME.Focus();
                }
                else if (INP_PHONE.Text == "")
                {
                    MessageBox.Show("Veuillez rentrer le téléphone fixe du collaborateur à créer.");
                    INP_PHONE.Focus();
                }
                else if (INP_CELLPHONE.Text == "")
                {
                    MessageBox.Show("Veuillez rentrer le téléphone portable du collaborateur à créer.");
                    INP_CELLPHONE.Focus();
                }
                else if (INP_MAIL.Text == "")
                {
                    MessageBox.Show("Veuillez rentrer le mail du collaborateur à créer.");
                    INP_MAIL.Focus();
                }
                else if (CMB_SERVICE.Text == "")
                {
                    MessageBox.Show("Veuillez rentrer le service du collaborateur à créer.");
                    CMB_SERVICE.Focus();
                }
                else if (CMB_SITE.Text == "")
                {
                    MessageBox.Show("Veuillez rentrer le site du collaborateur à créer.");
                    CMB_SITE.Focus();
                }
                else
                {
                    // Créer un collaborateur
                    CollaboratorDAO collaborator = new CollaboratorDAO();
                    collaborator.Firstname = INP_FIRSTNAME.Text.ToString();
                    collaborator.Lastname = INP_LASTNAME.Text.ToString();
                    collaborator.Phone = INP_PHONE.Text.ToString();
                    collaborator.Cellphone = INP_CELLPHONE.Text.ToString();
                    collaborator.Mail = INP_MAIL.Text.ToString();
                    collaborator.Service = int.Parse(CMB_SERVICE.Text.ToString().Split(" | ")[1]);
                    collaborator.Site = int.Parse(CMB_SITE.Text.ToString().Split(" | ")[1]);
                    collaborator.CreatedAt = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
                    collaborator.UpdatedAt = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);

                    // Appelle la procédure pour créer un collaborateur
                    if (collaborator.AddCollaborator())
                    {
                        // Affiche un toast à l'utilisateur et ferme la fenêtre
                        new ToastContentBuilder()
                           .AddText("La création du collaborateur a réussie.")
                           .SetToastDuration(ToastDuration.Short)
                           .Show();
                        this.Close();
                    }
                    else
                    { 
                        // Affiche un message d'erreur et ferme la fenêtre
                        MessageBox.Show("La création du collaborateur a échouée.", "Echec création collaborateur");
                    }
                }
            }
            else
            {
                // Ferme la fenêtre
                WIN_COLLABORATOR.ActiveForm.Close();
            }
        }

        private void BTN_DELETE_Click(object sender, EventArgs e)
        {
            // Récupère l'utilsiateur passé en paramètres
            CollaboratorDAO collaborator = CollaboratorDAO.GetCollaborator(this.idCollaborator).collaborator;

            // Appelle la fonction pour supprimer un utilisateur
            if (collaborator.DeleteCollaborator())
            {
                // Affiche un toast à l'utilisateur et ferme la fenêtre
                new ToastContentBuilder()
                   .AddText("La suppression du collaborateur a réussie.")
                   .SetToastDuration(ToastDuration.Short)
                   .Show();
                this.Close();
            }
            else
            {
                // Affiche un message d'erreur
                MessageBox.Show("La suppression du collaborateur a échouée.", "Echec suppression collaborateur");
            }
        }

        private void BTN_UPDATE_Click(object sender, EventArgs e)
        {
            // Récupère l'utilsiateur passé en paramètres
            CollaboratorDAO collaborator = CollaboratorDAO.GetCollaborator(this.idCollaborator).collaborator;

            // Vérifie que les champs soient complétés
            if (INP_FIRSTNAME.Text == "")
            {
                MessageBox.Show("Veuillez rentrer le prénom du collaborateur à créer.");
                INP_FIRSTNAME.Focus();
            }
            else if (INP_LASTNAME.Text == "")
            {
                MessageBox.Show("Veuillez rentrer le nom du collaborateur à créer.");
                INP_LASTNAME.Focus();
            }
            else if (INP_PHONE.Text == "")
            {
                MessageBox.Show("Veuillez rentrer le téléphone fixe du collaborateur à créer.");
                INP_PHONE.Focus();
            }
            else if (INP_CELLPHONE.Text == "")
            {
                MessageBox.Show("Veuillez rentrer le téléphone portable du collaborateur à créer.");
                INP_CELLPHONE.Focus();
            }
            else if (INP_MAIL.Text == "")
            {
                MessageBox.Show("Veuillez rentrer le mail du collaborateur à créer.");
                INP_MAIL.Focus();
            }
            else if (CMB_SERVICE.Text == "")
            {
                MessageBox.Show("Veuillez rentrer le service du collaborateur à créer.");
                CMB_SERVICE.Focus();
            }
            else if (CMB_SITE.Text == "")
            {
                MessageBox.Show("Veuillez rentrer le site du collaborateur à créer.");
                CMB_SITE.Focus();
            }
            else
            {
                // Met à jour les données du collaborateur
                collaborator.Firstname = INP_FIRSTNAME.Text.ToString();
                collaborator.Lastname = INP_LASTNAME.Text.ToString();
                collaborator.Phone = INP_PHONE.Text.ToString();
                collaborator.Cellphone = INP_CELLPHONE.Text.ToString();
                collaborator.Mail = INP_MAIL.Text.ToString();
                collaborator.Service = int.Parse(CMB_SERVICE.Text.ToString().Split(" | ")[1]);
                collaborator.Site = int.Parse(CMB_SITE.Text.ToString().Split(" | ")[1]);
                collaborator.UpdatedAt = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);

                // Appelle la procédure pour mettre à jour un collaborateur
                if (collaborator.UpdateCollaborator())
                {
                    // Affiche un toast à l'utilisateur et ferme la fenêtre
                    new ToastContentBuilder()
                       .AddText("La modification du collaborateur a réussie.")
                       .SetToastDuration(ToastDuration.Short)
                       .Show();
                    this.Close();
                }
                else
                {
                    // Affiche un message d'erreur
                    MessageBox.Show("La mise à jour du collaborateur a échouée.", "Echec mis à jour collaborateur");
                }
            }
        }
    }
}

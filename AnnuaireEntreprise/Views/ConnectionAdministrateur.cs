﻿using Microsoft.Toolkit.Uwp.Notifications;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnnuaireEntreprise
{
    public partial class WIN_CONNECTION : Form
    {
        private WIN_HOME home;
        public WIN_CONNECTION(WIN_HOME Home)
        {
            InitializeComponent();
            this.home = Home;
        }

        private void BTN_CANCEL_Click(object sender, EventArgs e)
        {
            // Passe l'état admin de la fenêtre à vrai
            home.adminState = false;
            // Ferme la fenêtre
            this.Close();
        }

        private void BTN_OK_Click(object sender, EventArgs e)
        {
            // Vérifie si le mot de passe saisi est valide
            if (INP_PWD.Text == "password")
            {
                // Affiche un toast à l'utilisateur
                new ToastContentBuilder()
                   .AddText("Vous êtes maintenant connecté en administrateur.")
                   .SetToastDuration(ToastDuration.Short)
                   .Show();

                // Passe l'état admin de la fenêtre à vrai
                home.adminState = true;
                // Ferme la fenêtre
                this.Close();
            }
            else
            {
                // Affiche un message d'erreur à l'utilisateur
                MessageBox.Show("Le mot de passe saisi est incorrect.", "Erreur mot de passe");
                // Place le curseur sur le champ MOT DE PASSE
                INP_PWD.Focus();
                INP_PWD.SelectAll();
            }
        }
    }
}

using AnnuaireEntreprise.Controllers;
using AnnuaireEntreprise.Models;
using System.CodeDom;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Cryptography.X509Certificates;

namespace AnnuaireEntreprise
{
    public partial class WIN_HOME : Form
    {
        public bool adminState;
        public WIN_HOME(bool AdminState = false)
        {
            InitializeComponent();
            this.adminState = AdminState;
        }

        public void ReloadCombos()
        {
            // Vide les combos
            CMB_SERVICE.Items.Clear();
            CMB_SITE.Items.Clear();

            // R�cup�re l'ensemble des sites disponibles
            ArrayList tabSites = new ArrayList();
            tabSites = SiteDAO.GetSites();

            // Parcourt tous les sites r�cup�r�s
            foreach (SiteDAO site in tabSites)
            {
                // Ajoute le site � la liste d�roulante
                CMB_SITE.Items.Add(site.City + " | " + site.Id);
            }

            // R�cup�re l'ensemble des services disponibles
            ArrayList tabServices = new ArrayList();
            tabServices = ServiceDAO.GetServices();

            // Parcourt tous les services r�cup�r�s
            foreach (ServiceDAO service in tabServices)
            {
                // Ajoute le service � la liste d�roulante
                CMB_SERVICE.Items.Add(service.Name + " | " + service.Id);
            }
        }

        private void Home_Load(object sender, EventArgs e)
        {
            // Masque les options de menu Gestion
            TSMI_COLLABORATORS.Enabled = false;
            TSMI_SERVICE.Enabled = false;
            TSMI_SITE.Enabled = false;

            // Appelle la fonction pour recharger les combo
            this.ReloadCombos();            

            // R�cup�re les utilisateurs disponibles
            ArrayList tabCollaborators = new ArrayList();
            tabCollaborators = CollaboratorDAO.GetCollaborators(new SearchCollaborator());

            // Afffiche un tableau avec les utilisateurs disponibles
            foreach (CollaboratorDAO collaborator in tabCollaborators)
            {
                // R�cup�re le nom du service et du site
                ServiceDAO service = (ServiceDAO.GetService((int)collaborator.Service)).service;

                SiteDAO site = (SiteDAO.GetSite((int)collaborator.Site)).site;

                // Pr�pare la ligne de donn�es
                string[] dataRow = new string[]
                {
                    collaborator.Id.ToString(),
                    collaborator.Firstname,
                    collaborator.Lastname,
                    collaborator.Phone,
                    collaborator.Cellphone,
                    collaborator.Mail,
                    service.Name,
                    site.City,
                    collaborator.CreatedAt.ToString(),
                    collaborator.UpdatedAt.ToString()
                };
                // Ajoute une ligne � la Grid_View
                DGV_COLLABORATORS.Rows.Add(dataRow);
            }
        }

        private void DGV_COLLABORATORS_DoubleClick(object sender, EventArgs e)
        {
            // V�rifie qu'une seule ligne est s�lectionn�e
            if (DGV_COLLABORATORS.SelectedRows.Count == 1)
            {
                // Si on arrive � r�cup�re l'utilisateur s�lectionn�
                if (int.TryParse(DGV_COLLABORATORS.SelectedCells[0].Value.ToString(), out int idCollaborator))
                {
                    // On affiche la feuille de pr�sentation du collaborateur en fonction du statut
                    if (this.adminState)
                    {
                        WIN_COLLABORATOR winCollaborator = new WIN_COLLABORATOR(idCollaborator, 2);
                        winCollaborator.ShowDialog();
                    }
                    else
                    {
                        WIN_COLLABORATOR winCollaborator = new WIN_COLLABORATOR(idCollaborator);
                        winCollaborator.ShowDialog();
                    }
                }
            }
        }

        private void TSMI_CANCEL_Click(object sender, EventArgs e)
        {
            // Quitte l'application
            WIN_HOME.ActiveForm.Close();
        }

        private void TSMI_COLLAB_ADD_Click(object sender, EventArgs e)
        {
            // Cr�� un objet Collaborator et ouvre la fen�tre pour le cr�er
            WIN_COLLABORATOR winCollaborator = new WIN_COLLABORATOR(-1, 3);
            winCollaborator.ShowDialog();

        }

        private void TSMI_ADMIN_Click(object sender, EventArgs e)
        {
            // V�rifie que l'�tat administrateur est activ�
            if (adminState)
            {
                // Modifie le titre de la fen�tre
                this.Text = "Annuaire Entreprise";
                // Modifie le menu administrateur
                TSMI_ADMIN.Text = "Connexion Admin";
                // Masque les options de menu Gestion
                TSMI_COLLABORATORS.Enabled = false;
                TSMI_SERVICE.Enabled = false;
                TSMI_SITE.Enabled = false;
                // Passe l'�tat admin � faux
                this.adminState = false;
            }
            else
            {
                // Demande la connexion administarteur
                WIN_CONNECTION winConnection = new WIN_CONNECTION(this);
                winConnection.ShowDialog();
                // Si l'�tat admin a �t� activ�
                if (adminState)
                {
                    // Modifie le titre de la fen�tre
                    this.Text = "Annuaire Entreprise - Administrateur";
                    // Modifie le menu administrateur
                    TSMI_ADMIN.Text = "D�connexion Admin";
                    // Autorise les options de menu Gestion
                    TSMI_COLLABORATORS.Enabled = true;
                    TSMI_SERVICE.Enabled = true;
                    TSMI_SITE.Enabled = true;
                }
            }
        }

        private void BTN_SEARCH_Click(object sender, EventArgs e)
        {
            // R�cup�re les informations de recherche
            string Firstname = INP_FIRSTNAME.Text;
            string Lastname = INP_LASTNAME.Text;
            string Service = CMB_SERVICE.Text;
            int idService = 0;
            if (Service != "")
            {
                idService = int.Parse((Service.Split(" | "))[1]);
            }
            string Site = CMB_SITE.Text;
            int idSite = 0;
            if (Site != "")
            {
                idSite = int.Parse((Site.Split(" | "))[1]);
            }

            // R�cup�re les utilisateurs disponibles
            ArrayList tabCollaborators = new ArrayList();
            // Cr�e l'objet avec les param�tres de la requ�te
            SearchCollaborator collaboratorParameters = new SearchCollaborator();
            collaboratorParameters.firstname = Firstname;
            collaboratorParameters.lastname = Lastname;
            collaboratorParameters.idService = idService;
            collaboratorParameters.idSite = idSite;

            // Recherche les collaborateurs correspondants
            tabCollaborators = CollaboratorDAO.GetCollaborators(collaboratorParameters);

            // Vide le tableau de recherche
            DGV_COLLABORATORS.Rows.Clear();

            // Afffiche un tableau avec les utilisateurs disponibles
            foreach (CollaboratorDAO collaborator in tabCollaborators)
            {
                // R�cup�re le nom du service et du site
                ServiceDAO service = ServiceDAO.GetService((int) collaborator.Service).service;

                SiteDAO site = SiteDAO.GetSite((int) collaborator.Site).site;

                // Pr�pare la ligne de donn�es
                string[] dataRow = new string[] 
                {
                    collaborator.Id.ToString(),
                    collaborator.Firstname,
                    collaborator.Lastname,
                    collaborator.Phone,
                    collaborator.Cellphone,
                    collaborator.Mail,
                    service.Name,
                    site.City,
                    collaborator.CreatedAt.ToString(),
                    collaborator.UpdatedAt.ToString()
                };
                // Ajoute une ligne � la Grid_View
                DGV_COLLABORATORS.Rows.Add(dataRow);
            }

            // Vide les champs de recherche
            INP_FIRSTNAME.Text = "";
            INP_LASTNAME.Text = "";
            CMB_SERVICE.Text = "";
            CMB_SITE.Text = "";
        }

        private void TSMI_SERVICE_Click(object sender, EventArgs e)
        {
            // Affiche la fen�tre pour g�rer les service
            WIN_SERVICE service = new WIN_SERVICE();
            service.ShowDialog();
            // Appelle la fonction pour recharger les combo
            this.ReloadCombos();
        }

        private void TSMI_SITE_Click(object sender, EventArgs e)
        {
            // Affiche la fen�tre pour g�rer les service
            WIN_SITE site = new WIN_SITE();
            site.ShowDialog();
            // Appelle la fonction pour recharger les combo
            this.ReloadCombos();
        }
    }
}
﻿namespace AnnuaireEntreprise
{
    partial class WIN_COLLABORATOR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WIN_COLLABORATOR));
            this.LBL_COLLABORATOR = new System.Windows.Forms.Label();
            this.LBL_FIRSTNAME = new System.Windows.Forms.Label();
            this.LBL_LASTNAME = new System.Windows.Forms.Label();
            this.LBL_PHONE = new System.Windows.Forms.Label();
            this.LBL_CELLPHONE = new System.Windows.Forms.Label();
            this.LBL_MAIL = new System.Windows.Forms.Label();
            this.LBL_SERVICE = new System.Windows.Forms.Label();
            this.LBL_SITE = new System.Windows.Forms.Label();
            this.BTN_DELETE = new System.Windows.Forms.Button();
            this.BTN_CANCEL = new System.Windows.Forms.Button();
            this.INP_FIRSTNAME = new System.Windows.Forms.TextBox();
            this.INP_LASTNAME = new System.Windows.Forms.TextBox();
            this.INP_PHONE = new System.Windows.Forms.TextBox();
            this.INP_CELLPHONE = new System.Windows.Forms.TextBox();
            this.INP_MAIL = new System.Windows.Forms.TextBox();
            this.CMB_SERVICE = new System.Windows.Forms.ComboBox();
            this.CMB_SITE = new System.Windows.Forms.ComboBox();
            this.BTN_UPDATE = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LBL_COLLABORATOR
            // 
            this.LBL_COLLABORATOR.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.LBL_COLLABORATOR.Location = new System.Drawing.Point(1, 9);
            this.LBL_COLLABORATOR.Name = "LBL_COLLABORATOR";
            this.LBL_COLLABORATOR.Size = new System.Drawing.Size(409, 27);
            this.LBL_COLLABORATOR.TabIndex = 0;
            this.LBL_COLLABORATOR.Text = "Fiche collaborateur xxx xxx";
            this.LBL_COLLABORATOR.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LBL_FIRSTNAME
            // 
            this.LBL_FIRSTNAME.AutoSize = true;
            this.LBL_FIRSTNAME.Location = new System.Drawing.Point(45, 57);
            this.LBL_FIRSTNAME.Name = "LBL_FIRSTNAME";
            this.LBL_FIRSTNAME.Size = new System.Drawing.Size(79, 23);
            this.LBL_FIRSTNAME.TabIndex = 1;
            this.LBL_FIRSTNAME.Text = "Prénom :";
            // 
            // LBL_LASTNAME
            // 
            this.LBL_LASTNAME.AutoSize = true;
            this.LBL_LASTNAME.Location = new System.Drawing.Point(67, 102);
            this.LBL_LASTNAME.Name = "LBL_LASTNAME";
            this.LBL_LASTNAME.Size = new System.Drawing.Size(57, 23);
            this.LBL_LASTNAME.TabIndex = 3;
            this.LBL_LASTNAME.Text = "Nom :";
            // 
            // LBL_PHONE
            // 
            this.LBL_PHONE.AutoSize = true;
            this.LBL_PHONE.Location = new System.Drawing.Point(50, 147);
            this.LBL_PHONE.Name = "LBL_PHONE";
            this.LBL_PHONE.Size = new System.Drawing.Size(74, 23);
            this.LBL_PHONE.TabIndex = 5;
            this.LBL_PHONE.Text = "Tél. fixe :";
            // 
            // LBL_CELLPHONE
            // 
            this.LBL_CELLPHONE.AutoSize = true;
            this.LBL_CELLPHONE.Location = new System.Drawing.Point(12, 192);
            this.LBL_CELLPHONE.Name = "LBL_CELLPHONE";
            this.LBL_CELLPHONE.Size = new System.Drawing.Size(112, 23);
            this.LBL_CELLPHONE.TabIndex = 7;
            this.LBL_CELLPHONE.Text = "Tél. portable :";
            // 
            // LBL_MAIL
            // 
            this.LBL_MAIL.AutoSize = true;
            this.LBL_MAIL.Location = new System.Drawing.Point(73, 237);
            this.LBL_MAIL.Name = "LBL_MAIL";
            this.LBL_MAIL.Size = new System.Drawing.Size(51, 23);
            this.LBL_MAIL.TabIndex = 9;
            this.LBL_MAIL.Text = "Mail :";
            // 
            // LBL_SERVICE
            // 
            this.LBL_SERVICE.AutoSize = true;
            this.LBL_SERVICE.Location = new System.Drawing.Point(52, 282);
            this.LBL_SERVICE.Name = "LBL_SERVICE";
            this.LBL_SERVICE.Size = new System.Drawing.Size(72, 23);
            this.LBL_SERVICE.TabIndex = 11;
            this.LBL_SERVICE.Text = "Service :";
            // 
            // LBL_SITE
            // 
            this.LBL_SITE.AutoSize = true;
            this.LBL_SITE.Location = new System.Drawing.Point(77, 328);
            this.LBL_SITE.Name = "LBL_SITE";
            this.LBL_SITE.Size = new System.Drawing.Size(47, 23);
            this.LBL_SITE.TabIndex = 13;
            this.LBL_SITE.Text = "Site :";
            // 
            // BTN_DELETE
            // 
            this.BTN_DELETE.Location = new System.Drawing.Point(64, 380);
            this.BTN_DELETE.Name = "BTN_DELETE";
            this.BTN_DELETE.Size = new System.Drawing.Size(124, 29);
            this.BTN_DELETE.TabIndex = 14;
            this.BTN_DELETE.Text = "Supprimer";
            this.BTN_DELETE.UseVisualStyleBackColor = true;
            this.BTN_DELETE.Click += new System.EventHandler(this.BTN_DELETE_Click);
            // 
            // BTN_CANCEL
            // 
            this.BTN_CANCEL.Location = new System.Drawing.Point(142, 380);
            this.BTN_CANCEL.Name = "BTN_CANCEL";
            this.BTN_CANCEL.Size = new System.Drawing.Size(124, 29);
            this.BTN_CANCEL.TabIndex = 15;
            this.BTN_CANCEL.Text = "Terminer";
            this.BTN_CANCEL.UseVisualStyleBackColor = true;
            this.BTN_CANCEL.Click += new System.EventHandler(this.BTN_CANCEL_Click);
            // 
            // INP_FIRSTNAME
            // 
            this.INP_FIRSTNAME.Location = new System.Drawing.Point(130, 53);
            this.INP_FIRSTNAME.Name = "INP_FIRSTNAME";
            this.INP_FIRSTNAME.Size = new System.Drawing.Size(264, 30);
            this.INP_FIRSTNAME.TabIndex = 2;
            // 
            // INP_LASTNAME
            // 
            this.INP_LASTNAME.Location = new System.Drawing.Point(130, 98);
            this.INP_LASTNAME.Name = "INP_LASTNAME";
            this.INP_LASTNAME.Size = new System.Drawing.Size(264, 30);
            this.INP_LASTNAME.TabIndex = 4;
            // 
            // INP_PHONE
            // 
            this.INP_PHONE.Location = new System.Drawing.Point(130, 143);
            this.INP_PHONE.Name = "INP_PHONE";
            this.INP_PHONE.Size = new System.Drawing.Size(264, 30);
            this.INP_PHONE.TabIndex = 5;
            // 
            // INP_CELLPHONE
            // 
            this.INP_CELLPHONE.Location = new System.Drawing.Point(130, 188);
            this.INP_CELLPHONE.Name = "INP_CELLPHONE";
            this.INP_CELLPHONE.Size = new System.Drawing.Size(264, 30);
            this.INP_CELLPHONE.TabIndex = 6;
            // 
            // INP_MAIL
            // 
            this.INP_MAIL.Location = new System.Drawing.Point(130, 233);
            this.INP_MAIL.Name = "INP_MAIL";
            this.INP_MAIL.Size = new System.Drawing.Size(264, 30);
            this.INP_MAIL.TabIndex = 8;
            // 
            // CMB_SERVICE
            // 
            this.CMB_SERVICE.FormattingEnabled = true;
            this.CMB_SERVICE.Location = new System.Drawing.Point(130, 278);
            this.CMB_SERVICE.Name = "CMB_SERVICE";
            this.CMB_SERVICE.Size = new System.Drawing.Size(264, 31);
            this.CMB_SERVICE.TabIndex = 10;
            // 
            // CMB_SITE
            // 
            this.CMB_SITE.FormattingEnabled = true;
            this.CMB_SITE.Location = new System.Drawing.Point(130, 324);
            this.CMB_SITE.Name = "CMB_SITE";
            this.CMB_SITE.Size = new System.Drawing.Size(264, 31);
            this.CMB_SITE.TabIndex = 12;
            // 
            // BTN_UPDATE
            // 
            this.BTN_UPDATE.Location = new System.Drawing.Point(220, 380);
            this.BTN_UPDATE.Name = "BTN_UPDATE";
            this.BTN_UPDATE.Size = new System.Drawing.Size(124, 29);
            this.BTN_UPDATE.TabIndex = 16;
            this.BTN_UPDATE.Text = "Modifier";
            this.BTN_UPDATE.UseVisualStyleBackColor = true;
            this.BTN_UPDATE.Click += new System.EventHandler(this.BTN_UPDATE_Click);
            // 
            // WIN_COLLABORATOR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 423);
            this.Controls.Add(this.CMB_SITE);
            this.Controls.Add(this.CMB_SERVICE);
            this.Controls.Add(this.INP_MAIL);
            this.Controls.Add(this.INP_CELLPHONE);
            this.Controls.Add(this.INP_PHONE);
            this.Controls.Add(this.INP_LASTNAME);
            this.Controls.Add(this.INP_FIRSTNAME);
            this.Controls.Add(this.BTN_UPDATE);
            this.Controls.Add(this.BTN_CANCEL);
            this.Controls.Add(this.BTN_DELETE);
            this.Controls.Add(this.LBL_SITE);
            this.Controls.Add(this.LBL_SERVICE);
            this.Controls.Add(this.LBL_MAIL);
            this.Controls.Add(this.LBL_CELLPHONE);
            this.Controls.Add(this.LBL_PHONE);
            this.Controls.Add(this.LBL_LASTNAME);
            this.Controls.Add(this.LBL_FIRSTNAME);
            this.Controls.Add(this.LBL_COLLABORATOR);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(427, 470);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(427, 470);
            this.Name = "WIN_COLLABORATOR";
            this.Text = "Collaborateurs";
            this.Load += new System.EventHandler(this.WIN_COLLABORATOR_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label LBL_COLLABORATOR;
        private Label LBL_FIRSTNAME;
        private Label LBL_LASTNAME;
        private Label LBL_PHONE;
        private Label LBL_CELLPHONE;
        private Label LBL_MAIL;
        private Label LBL_SERVICE;
        private Label LBL_SITE;
        private Button BTN_DELETE;
        private Button BTN_CANCEL;
        private TextBox INP_FIRSTNAME;
        private TextBox INP_LASTNAME;
        private TextBox INP_PHONE;
        private TextBox INP_CELLPHONE;
        private TextBox INP_MAIL;
        private ComboBox CMB_SERVICE;
        private ComboBox CMB_SITE;
        private Button BTN_UPDATE;
    }
}
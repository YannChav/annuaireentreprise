﻿namespace AnnuaireEntreprise
{
    partial class WIN_CONNECTION
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WIN_CONNECTION));
            this.LBL_PWD = new System.Windows.Forms.Label();
            this.INP_PWD = new System.Windows.Forms.TextBox();
            this.BTN_OK = new System.Windows.Forms.Button();
            this.BTN_CANCEL = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LBL_PWD
            // 
            this.LBL_PWD.AutoSize = true;
            this.LBL_PWD.Location = new System.Drawing.Point(30, 26);
            this.LBL_PWD.Name = "LBL_PWD";
            this.LBL_PWD.Size = new System.Drawing.Size(237, 23);
            this.LBL_PWD.TabIndex = 0;
            this.LBL_PWD.Text = "Mot de passe administrateur :";
            // 
            // INP_PWD
            // 
            this.INP_PWD.Location = new System.Drawing.Point(275, 22);
            this.INP_PWD.Name = "INP_PWD";
            this.INP_PWD.PasswordChar = '*';
            this.INP_PWD.Size = new System.Drawing.Size(219, 30);
            this.INP_PWD.TabIndex = 1;
            // 
            // BTN_OK
            // 
            this.BTN_OK.Location = new System.Drawing.Point(310, 70);
            this.BTN_OK.Name = "BTN_OK";
            this.BTN_OK.Size = new System.Drawing.Size(94, 29);
            this.BTN_OK.TabIndex = 2;
            this.BTN_OK.Text = "Valider";
            this.BTN_OK.UseVisualStyleBackColor = true;
            this.BTN_OK.Click += new System.EventHandler(this.BTN_OK_Click);
            // 
            // BTN_CANCEL
            // 
            this.BTN_CANCEL.Location = new System.Drawing.Point(121, 70);
            this.BTN_CANCEL.Name = "BTN_CANCEL";
            this.BTN_CANCEL.Size = new System.Drawing.Size(94, 29);
            this.BTN_CANCEL.TabIndex = 3;
            this.BTN_CANCEL.Text = "Annuler";
            this.BTN_CANCEL.UseVisualStyleBackColor = true;
            this.BTN_CANCEL.Click += new System.EventHandler(this.BTN_CANCEL_Click);
            // 
            // WIN_CONNECTION
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 112);
            this.Controls.Add(this.BTN_CANCEL);
            this.Controls.Add(this.BTN_OK);
            this.Controls.Add(this.INP_PWD);
            this.Controls.Add(this.LBL_PWD);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(543, 159);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(543, 159);
            this.Name = "WIN_CONNECTION";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Text = "Connexion Administrateur";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label LBL_PWD;
        private TextBox INP_PWD;
        private Button BTN_OK;
        private Button BTN_CANCEL;
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AnnuaireEntreprise.Models;
using Microsoft.Toolkit.Uwp.Notifications;

namespace AnnuaireEntreprise
{
    public partial class WIN_SITE : Form
    {
        public WIN_SITE()
        {
            InitializeComponent();
        }

        public void Reload()
        {
            // Vide la liste des sites
            CMB_SITE.Items.Clear();
            CMB_SITE.Text = "";
            // Récupère l'ensemble des services disponibles
            ArrayList tabSites = new ArrayList();
            tabSites = SiteDAO.GetSites();

            // Parcourt tous les services récupérés
            foreach (SiteDAO site in tabSites)
            {
                // Ajoute le service à la liste déroulante
                CMB_SITE.Items.Add(site.City + " | " + site.Id);
            }
            // Vide les champs
            INP_CONSULT_CITY.Text = "";
            INP_CONSULT_POSTCODE.Text = "";
            INP_CONSULT_ADRESS.Text = "";
            CMB_CONSULT_ROLE.Text = "";
            INP_ID.Text = "";
            INP_CRETAED_AT.Text = "";
            INP_UPDATED_AT.Text = "";

            INP_CREATE_CITY.Text = "";
            INP_CREATE_POSTCODE.Text = "";
            INP_CREATE_ADRESSE.Text = "";
            CMB_CREATE_ROLE.Text = "";
        }

        private void WIN_SERVICE_Load(object sender, EventArgs e)
        {
            // Appelle la fonction pour charger la page
            this.Reload();
        }

        private void BTN_FINISH_Click(object sender, EventArgs e)
        {
            // Quitte la fenêtre
            this.Close();
        }


        private void BTN_DELETE_Click(object sender, EventArgs e)
        {
            if (CMB_SITE.Text.ToString() != "")
            {
                // Récupère l'id du site selectionné
                int idSite = int.Parse((CMB_SITE.Text.ToString().Split(" | "))[1]);

                // Crée un objet Site
                SiteDAO site = new SiteDAO();
                site.Id = idSite;

                // Vérifie si le site est utilisé
                SearchCollaborator collaboratorParameters = new SearchCollaborator();
                collaboratorParameters.idSite = idSite;

                if (CollaboratorDAO.GetCollaborators(collaboratorParameters).Count > 0)
                {
                    // Affiche un message d'erreur à l'utilisateur
                    MessageBox.Show("Ce site ne peut pas être supprimé car des collaborateurs " +
                        "y travaillent.");
                }
                else
                {
                    // Vérifie si la suppression du site à bien eue lieu
                    if (!site.DeleteSite())
                    {
                        // Affiche un message d'erreur à l'utilisateur
                        MessageBox.Show("Une erreur s'est produite lors de la suppression du site.");
                    }
                    else
                    {
                        // Affiche un toast à l'utilisateur
                        new ToastContentBuilder()
                           .AddText("La suppression du site s'est correctement effectuée.")
                           .SetToastDuration(ToastDuration.Short)
                           .Show();
                    }
                }
            }
            else
            {
                MessageBox.Show("Veuillez sélectionner un site pour le supprimer.");
            }

            // Rafraîchit les données de la fenêtre
            this.Reload();
        }

        private void BTN_SAVE_Click(object sender, EventArgs e)
        {
            // Enregistre les modifications du service si les différents champs ne sont pas vides
            if (INP_CONSULT_CITY.Text != "" && INP_CONSULT_ADRESS.Text != "" && INP_CONSULT_POSTCODE.Text != "")
            {
                // Créé un objet Site et le met à jour
                SiteDAO site = SiteDAO.GetSite(int.Parse(INP_ID.Text)).site;
                site.City = INP_CONSULT_CITY.Text;
                site.Postcode = INP_CONSULT_POSTCODE.Text;
                site.Adress = INP_CONSULT_ADRESS.Text;
                site.UpdatedAt = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day); ;

                if (!site.UpdateSite())
                {
                    // Affiche un message d'erreur à l'utilisateur
                    MessageBox.Show("Une erreur s'est produite lors de la mise à jour du site.");
                }
                else
                {
                    // Affiche un toast à l'utilisateur
                    new ToastContentBuilder()
                       .AddText("La mise à jour du site s'est correctement effectuée.")
                       .SetToastDuration(ToastDuration.Short)
                       .Show();
                }

                // Rafraîchit les données de la fenêtre
                this.Reload();
            }
            else
            {
                // Affiche un message d'erreur à l'utilisateur
                MessageBox.Show("Veuillez renseigner un nom, une adresse et un code postal valide " +
                    "pour modifier un site.");
            }
        }

        private void BTN_CREATE_Click(object sender, EventArgs e)
        {
            // Vérifie que les champs nécessaires à la création d'un site soient renseignés
            if (INP_CREATE_CITY.Text != "" && INP_CREATE_ADRESSE.Text != "" && 
                INP_CREATE_POSTCODE.Text != "" && CMB_CREATE_ROLE.Text != "")
            {

                // Créé un objet Site et l'enregistre en BDD
                SiteDAO site = new SiteDAO();
                site.City = INP_CREATE_CITY.Text;
                site.Postcode = INP_CREATE_POSTCODE.Text;
                site.Adress = INP_CREATE_ADRESSE.Text;
                site.Role = CMB_CREATE_ROLE.Text;
                site.CreatedAt = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
                site.UpdatedAt = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);

                if (!site.AddSite())
                {
                    // Affiche un message d'erreur à l'utilisateur
                    MessageBox.Show("Une erreur s'est produite lors de la création du site.");
                }
                else
                {
                    // Affiche un toast à l'utilisateur
                    new ToastContentBuilder()
                       .AddText("La création du site s'est correctement effectuée.")
                       .SetToastDuration(ToastDuration.Short)
                       .Show();
                }

                // Rafraîchit les données de la fenêtre
                this.Reload();
            }
            else
            {
                // Affiche un message d'erreur à l'utilisateur
                MessageBox.Show("Veuillez renseigner une ville, une adresse, un code postal et un " +
                    "rôle de site valides pour créer un site.");
            }
        }

        private void CMB_SITE_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Récupère l'id du service selectionné
            int idSite = int.Parse((CMB_SITE.Text.ToString().Split(" | "))[1]);

            // Crée un objet
            (bool state, SiteDAO site) resultGetSite = SiteDAO.GetSite(idSite);

            // Récupère les informations de l'objet Site
            if (resultGetSite.state)
            {
                SiteDAO site = resultGetSite.site;
                // Affiche les informations du service dans la fenêtre
                INP_CONSULT_CITY.Text = site.City;
                INP_CONSULT_POSTCODE.Text = site.Postcode;
                INP_CONSULT_ADRESS.Text = site.Adress;
                CMB_CONSULT_ROLE.Text = site.Role;
                INP_ID.Text = site.Id.ToString();
                INP_CRETAED_AT.Text = site.CreatedAt.ToString();
                INP_UPDATED_AT.Text = site.UpdatedAt.ToString();
            }
            else
            {
                // Affiche un message d'erreur à l'utilisateur
                MessageBox.Show("Les données du site sélectionné n'ont pas pu être récupérées.");
                // Recharge les données de la fenêtre
                this.Reload();
            }
        }
    }
}

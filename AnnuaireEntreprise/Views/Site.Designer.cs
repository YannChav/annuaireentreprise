﻿namespace AnnuaireEntreprise
{
    partial class WIN_SITE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WIN_SITE));
            this.LBL_CONSULT_CITY = new System.Windows.Forms.Label();
            this.CMB_SITE = new System.Windows.Forms.ComboBox();
            this.GB_CONSULT_SITE = new System.Windows.Forms.GroupBox();
            this.CMB_CONSULT_ROLE = new System.Windows.Forms.ComboBox();
            this.LBL_CONSULT_ROLE = new System.Windows.Forms.Label();
            this.INP_CONSULT_POSTCODE = new System.Windows.Forms.TextBox();
            this.LBL_CONSULT_POSTCODE = new System.Windows.Forms.Label();
            this.INP_CONSULT_ADRESS = new System.Windows.Forms.TextBox();
            this.LBL_CONSULT_ADRESS = new System.Windows.Forms.Label();
            this.INP_CRETAED_AT = new System.Windows.Forms.TextBox();
            this.LBL_CREATED_AT = new System.Windows.Forms.Label();
            this.INP_UPDATED_AT = new System.Windows.Forms.TextBox();
            this.LBL_UPDATED_AT = new System.Windows.Forms.Label();
            this.INP_ID = new System.Windows.Forms.TextBox();
            this.LBL_ID = new System.Windows.Forms.Label();
            this.BTN_SAVE = new System.Windows.Forms.Button();
            this.BTN_DELETE = new System.Windows.Forms.Button();
            this.INP_CONSULT_CITY = new System.Windows.Forms.TextBox();
            this.GB_CREATE_SERVICE = new System.Windows.Forms.GroupBox();
            this.INP_CREATE_POSTCODE = new System.Windows.Forms.TextBox();
            this.LBL_CREATE_POSTCODE = new System.Windows.Forms.Label();
            this.CMB_CREATE_ROLE = new System.Windows.Forms.ComboBox();
            this.LBL_CREATE_ROLE = new System.Windows.Forms.Label();
            this.INP_CREATE_ADRESSE = new System.Windows.Forms.TextBox();
            this.LBL_CREATE_ADRESS = new System.Windows.Forms.Label();
            this.BTN_CREATE = new System.Windows.Forms.Button();
            this.INP_CREATE_CITY = new System.Windows.Forms.TextBox();
            this.LBL_CREATE_CITY = new System.Windows.Forms.Label();
            this.BTN_FINISH = new System.Windows.Forms.Button();
            this.GB_CONSULT_SITE.SuspendLayout();
            this.GB_CREATE_SERVICE.SuspendLayout();
            this.SuspendLayout();
            // 
            // LBL_CONSULT_CITY
            // 
            this.LBL_CONSULT_CITY.AutoSize = true;
            this.LBL_CONSULT_CITY.Location = new System.Drawing.Point(33, 89);
            this.LBL_CONSULT_CITY.Name = "LBL_CONSULT_CITY";
            this.LBL_CONSULT_CITY.Size = new System.Drawing.Size(51, 23);
            this.LBL_CONSULT_CITY.TabIndex = 0;
            this.LBL_CONSULT_CITY.Text = "Ville :";
            // 
            // CMB_SITE
            // 
            this.CMB_SITE.FormattingEnabled = true;
            this.CMB_SITE.Location = new System.Drawing.Point(67, 29);
            this.CMB_SITE.Name = "CMB_SITE";
            this.CMB_SITE.Size = new System.Drawing.Size(266, 31);
            this.CMB_SITE.TabIndex = 2;
            this.CMB_SITE.SelectedIndexChanged += new System.EventHandler(this.CMB_SITE_SelectedIndexChanged);
            // 
            // GB_CONSULT_SITE
            // 
            this.GB_CONSULT_SITE.Controls.Add(this.CMB_CONSULT_ROLE);
            this.GB_CONSULT_SITE.Controls.Add(this.LBL_CONSULT_ROLE);
            this.GB_CONSULT_SITE.Controls.Add(this.INP_CONSULT_POSTCODE);
            this.GB_CONSULT_SITE.Controls.Add(this.LBL_CONSULT_POSTCODE);
            this.GB_CONSULT_SITE.Controls.Add(this.INP_CONSULT_ADRESS);
            this.GB_CONSULT_SITE.Controls.Add(this.LBL_CONSULT_ADRESS);
            this.GB_CONSULT_SITE.Controls.Add(this.INP_CRETAED_AT);
            this.GB_CONSULT_SITE.Controls.Add(this.LBL_CREATED_AT);
            this.GB_CONSULT_SITE.Controls.Add(this.INP_UPDATED_AT);
            this.GB_CONSULT_SITE.Controls.Add(this.LBL_UPDATED_AT);
            this.GB_CONSULT_SITE.Controls.Add(this.INP_ID);
            this.GB_CONSULT_SITE.Controls.Add(this.LBL_ID);
            this.GB_CONSULT_SITE.Controls.Add(this.BTN_SAVE);
            this.GB_CONSULT_SITE.Controls.Add(this.BTN_DELETE);
            this.GB_CONSULT_SITE.Controls.Add(this.INP_CONSULT_CITY);
            this.GB_CONSULT_SITE.Controls.Add(this.CMB_SITE);
            this.GB_CONSULT_SITE.Controls.Add(this.LBL_CONSULT_CITY);
            this.GB_CONSULT_SITE.Location = new System.Drawing.Point(12, 12);
            this.GB_CONSULT_SITE.Name = "GB_CONSULT_SITE";
            this.GB_CONSULT_SITE.Size = new System.Drawing.Size(470, 317);
            this.GB_CONSULT_SITE.TabIndex = 3;
            this.GB_CONSULT_SITE.TabStop = false;
            this.GB_CONSULT_SITE.Text = "Consultation";
            // 
            // CMB_CONSULT_ROLE
            // 
            this.CMB_CONSULT_ROLE.Enabled = false;
            this.CMB_CONSULT_ROLE.FormattingEnabled = true;
            this.CMB_CONSULT_ROLE.Items.AddRange(new object[] {
            "Production*",
            "Administratif"});
            this.CMB_CONSULT_ROLE.Location = new System.Drawing.Point(90, 170);
            this.CMB_CONSULT_ROLE.Name = "CMB_CONSULT_ROLE";
            this.CMB_CONSULT_ROLE.Size = new System.Drawing.Size(230, 31);
            this.CMB_CONSULT_ROLE.TabIndex = 18;
            // 
            // LBL_CONSULT_ROLE
            // 
            this.LBL_CONSULT_ROLE.AutoSize = true;
            this.LBL_CONSULT_ROLE.Location = new System.Drawing.Point(32, 174);
            this.LBL_CONSULT_ROLE.Name = "LBL_CONSULT_ROLE";
            this.LBL_CONSULT_ROLE.Size = new System.Drawing.Size(52, 23);
            this.LBL_CONSULT_ROLE.TabIndex = 17;
            this.LBL_CONSULT_ROLE.Text = "Rôle :";
            // 
            // INP_CONSULT_POSTCODE
            // 
            this.INP_CONSULT_POSTCODE.Location = new System.Drawing.Point(388, 85);
            this.INP_CONSULT_POSTCODE.Name = "INP_CONSULT_POSTCODE";
            this.INP_CONSULT_POSTCODE.Size = new System.Drawing.Size(76, 30);
            this.INP_CONSULT_POSTCODE.TabIndex = 16;
            // 
            // LBL_CONSULT_POSTCODE
            // 
            this.LBL_CONSULT_POSTCODE.AutoSize = true;
            this.LBL_CONSULT_POSTCODE.Location = new System.Drawing.Point(342, 89);
            this.LBL_CONSULT_POSTCODE.Name = "LBL_CONSULT_POSTCODE";
            this.LBL_CONSULT_POSTCODE.Size = new System.Drawing.Size(40, 23);
            this.LBL_CONSULT_POSTCODE.TabIndex = 15;
            this.LBL_CONSULT_POSTCODE.Text = "CP :";
            // 
            // INP_CONSULT_ADRESS
            // 
            this.INP_CONSULT_ADRESS.Location = new System.Drawing.Point(90, 125);
            this.INP_CONSULT_ADRESS.Name = "INP_CONSULT_ADRESS";
            this.INP_CONSULT_ADRESS.Size = new System.Drawing.Size(374, 30);
            this.INP_CONSULT_ADRESS.TabIndex = 14;
            // 
            // LBL_CONSULT_ADRESS
            // 
            this.LBL_CONSULT_ADRESS.AutoSize = true;
            this.LBL_CONSULT_ADRESS.Location = new System.Drawing.Point(6, 129);
            this.LBL_CONSULT_ADRESS.Name = "LBL_CONSULT_ADRESS";
            this.LBL_CONSULT_ADRESS.Size = new System.Drawing.Size(78, 23);
            this.LBL_CONSULT_ADRESS.TabIndex = 13;
            this.LBL_CONSULT_ADRESS.Text = "Adresse :";
            // 
            // INP_CRETAED_AT
            // 
            this.INP_CRETAED_AT.Enabled = false;
            this.INP_CRETAED_AT.Location = new System.Drawing.Point(101, 219);
            this.INP_CRETAED_AT.Name = "INP_CRETAED_AT";
            this.INP_CRETAED_AT.Size = new System.Drawing.Size(95, 30);
            this.INP_CRETAED_AT.TabIndex = 12;
            // 
            // LBL_CREATED_AT
            // 
            this.LBL_CREATED_AT.AutoSize = true;
            this.LBL_CREATED_AT.Location = new System.Drawing.Point(23, 223);
            this.LBL_CREATED_AT.Name = "LBL_CREATED_AT";
            this.LBL_CREATED_AT.Size = new System.Drawing.Size(72, 23);
            this.LBL_CREATED_AT.TabIndex = 11;
            this.LBL_CREATED_AT.Text = "Créé le :";
            // 
            // INP_UPDATED_AT
            // 
            this.INP_UPDATED_AT.Enabled = false;
            this.INP_UPDATED_AT.Location = new System.Drawing.Point(353, 219);
            this.INP_UPDATED_AT.Name = "INP_UPDATED_AT";
            this.INP_UPDATED_AT.Size = new System.Drawing.Size(95, 30);
            this.INP_UPDATED_AT.TabIndex = 10;
            // 
            // LBL_UPDATED_AT
            // 
            this.LBL_UPDATED_AT.AutoSize = true;
            this.LBL_UPDATED_AT.Location = new System.Drawing.Point(254, 223);
            this.LBL_UPDATED_AT.Name = "LBL_UPDATED_AT";
            this.LBL_UPDATED_AT.Size = new System.Drawing.Size(94, 23);
            this.LBL_UPDATED_AT.TabIndex = 9;
            this.LBL_UPDATED_AT.Text = "Modifié le :";
            // 
            // INP_ID
            // 
            this.INP_ID.Enabled = false;
            this.INP_ID.Location = new System.Drawing.Point(388, 170);
            this.INP_ID.Name = "INP_ID";
            this.INP_ID.Size = new System.Drawing.Size(76, 30);
            this.INP_ID.TabIndex = 8;
            // 
            // LBL_ID
            // 
            this.LBL_ID.AutoSize = true;
            this.LBL_ID.Location = new System.Drawing.Point(339, 174);
            this.LBL_ID.Name = "LBL_ID";
            this.LBL_ID.Size = new System.Drawing.Size(36, 23);
            this.LBL_ID.TabIndex = 7;
            this.LBL_ID.Text = "ID :";
            // 
            // BTN_SAVE
            // 
            this.BTN_SAVE.Location = new System.Drawing.Point(254, 269);
            this.BTN_SAVE.Name = "BTN_SAVE";
            this.BTN_SAVE.Size = new System.Drawing.Size(112, 35);
            this.BTN_SAVE.TabIndex = 6;
            this.BTN_SAVE.Text = "Enregistrer";
            this.BTN_SAVE.UseVisualStyleBackColor = true;
            this.BTN_SAVE.Click += new System.EventHandler(this.BTN_SAVE_Click);
            // 
            // BTN_DELETE
            // 
            this.BTN_DELETE.Location = new System.Drawing.Point(105, 269);
            this.BTN_DELETE.Name = "BTN_DELETE";
            this.BTN_DELETE.Size = new System.Drawing.Size(112, 35);
            this.BTN_DELETE.TabIndex = 5;
            this.BTN_DELETE.Text = "Supprimer";
            this.BTN_DELETE.UseVisualStyleBackColor = true;
            this.BTN_DELETE.Click += new System.EventHandler(this.BTN_DELETE_Click);
            // 
            // INP_CONSULT_CITY
            // 
            this.INP_CONSULT_CITY.Location = new System.Drawing.Point(90, 85);
            this.INP_CONSULT_CITY.Name = "INP_CONSULT_CITY";
            this.INP_CONSULT_CITY.Size = new System.Drawing.Size(230, 30);
            this.INP_CONSULT_CITY.TabIndex = 4;
            // 
            // GB_CREATE_SERVICE
            // 
            this.GB_CREATE_SERVICE.Controls.Add(this.INP_CREATE_POSTCODE);
            this.GB_CREATE_SERVICE.Controls.Add(this.LBL_CREATE_POSTCODE);
            this.GB_CREATE_SERVICE.Controls.Add(this.CMB_CREATE_ROLE);
            this.GB_CREATE_SERVICE.Controls.Add(this.LBL_CREATE_ROLE);
            this.GB_CREATE_SERVICE.Controls.Add(this.INP_CREATE_ADRESSE);
            this.GB_CREATE_SERVICE.Controls.Add(this.LBL_CREATE_ADRESS);
            this.GB_CREATE_SERVICE.Controls.Add(this.BTN_CREATE);
            this.GB_CREATE_SERVICE.Controls.Add(this.INP_CREATE_CITY);
            this.GB_CREATE_SERVICE.Controls.Add(this.LBL_CREATE_CITY);
            this.GB_CREATE_SERVICE.Location = new System.Drawing.Point(488, 13);
            this.GB_CREATE_SERVICE.Name = "GB_CREATE_SERVICE";
            this.GB_CREATE_SERVICE.Size = new System.Drawing.Size(477, 223);
            this.GB_CREATE_SERVICE.TabIndex = 4;
            this.GB_CREATE_SERVICE.TabStop = false;
            this.GB_CREATE_SERVICE.Text = "Création";
            // 
            // INP_CREATE_POSTCODE
            // 
            this.INP_CREATE_POSTCODE.Location = new System.Drawing.Point(388, 39);
            this.INP_CREATE_POSTCODE.Name = "INP_CREATE_POSTCODE";
            this.INP_CREATE_POSTCODE.Size = new System.Drawing.Size(76, 30);
            this.INP_CREATE_POSTCODE.TabIndex = 24;
            // 
            // LBL_CREATE_POSTCODE
            // 
            this.LBL_CREATE_POSTCODE.AutoSize = true;
            this.LBL_CREATE_POSTCODE.Location = new System.Drawing.Point(342, 43);
            this.LBL_CREATE_POSTCODE.Name = "LBL_CREATE_POSTCODE";
            this.LBL_CREATE_POSTCODE.Size = new System.Drawing.Size(40, 23);
            this.LBL_CREATE_POSTCODE.TabIndex = 23;
            this.LBL_CREATE_POSTCODE.Text = "CP :";
            // 
            // CMB_CREATE_ROLE
            // 
            this.CMB_CREATE_ROLE.FormattingEnabled = true;
            this.CMB_CREATE_ROLE.Items.AddRange(new object[] {
            "Production",
            "Administratif"});
            this.CMB_CREATE_ROLE.Location = new System.Drawing.Point(90, 125);
            this.CMB_CREATE_ROLE.Name = "CMB_CREATE_ROLE";
            this.CMB_CREATE_ROLE.Size = new System.Drawing.Size(230, 31);
            this.CMB_CREATE_ROLE.TabIndex = 22;
            // 
            // LBL_CREATE_ROLE
            // 
            this.LBL_CREATE_ROLE.AutoSize = true;
            this.LBL_CREATE_ROLE.Location = new System.Drawing.Point(32, 129);
            this.LBL_CREATE_ROLE.Name = "LBL_CREATE_ROLE";
            this.LBL_CREATE_ROLE.Size = new System.Drawing.Size(52, 23);
            this.LBL_CREATE_ROLE.TabIndex = 21;
            this.LBL_CREATE_ROLE.Text = "Rôle :";
            // 
            // INP_CREATE_ADRESSE
            // 
            this.INP_CREATE_ADRESSE.Location = new System.Drawing.Point(90, 80);
            this.INP_CREATE_ADRESSE.Name = "INP_CREATE_ADRESSE";
            this.INP_CREATE_ADRESSE.Size = new System.Drawing.Size(374, 30);
            this.INP_CREATE_ADRESSE.TabIndex = 20;
            // 
            // LBL_CREATE_ADRESS
            // 
            this.LBL_CREATE_ADRESS.AutoSize = true;
            this.LBL_CREATE_ADRESS.Location = new System.Drawing.Point(6, 84);
            this.LBL_CREATE_ADRESS.Name = "LBL_CREATE_ADRESS";
            this.LBL_CREATE_ADRESS.Size = new System.Drawing.Size(78, 23);
            this.LBL_CREATE_ADRESS.TabIndex = 19;
            this.LBL_CREATE_ADRESS.Text = "Adresse :";
            // 
            // BTN_CREATE
            // 
            this.BTN_CREATE.Location = new System.Drawing.Point(182, 173);
            this.BTN_CREATE.Name = "BTN_CREATE";
            this.BTN_CREATE.Size = new System.Drawing.Size(112, 35);
            this.BTN_CREATE.TabIndex = 7;
            this.BTN_CREATE.Text = "Créer";
            this.BTN_CREATE.UseVisualStyleBackColor = true;
            this.BTN_CREATE.Click += new System.EventHandler(this.BTN_CREATE_Click);
            // 
            // INP_CREATE_CITY
            // 
            this.INP_CREATE_CITY.Location = new System.Drawing.Point(90, 40);
            this.INP_CREATE_CITY.Name = "INP_CREATE_CITY";
            this.INP_CREATE_CITY.Size = new System.Drawing.Size(230, 30);
            this.INP_CREATE_CITY.TabIndex = 5;
            // 
            // LBL_CREATE_CITY
            // 
            this.LBL_CREATE_CITY.AutoSize = true;
            this.LBL_CREATE_CITY.Location = new System.Drawing.Point(27, 42);
            this.LBL_CREATE_CITY.Name = "LBL_CREATE_CITY";
            this.LBL_CREATE_CITY.Size = new System.Drawing.Size(51, 23);
            this.LBL_CREATE_CITY.TabIndex = 1;
            this.LBL_CREATE_CITY.Text = "Ville :";
            // 
            // BTN_FINISH
            // 
            this.BTN_FINISH.Location = new System.Drawing.Point(856, 294);
            this.BTN_FINISH.Name = "BTN_FINISH";
            this.BTN_FINISH.Size = new System.Drawing.Size(112, 35);
            this.BTN_FINISH.TabIndex = 6;
            this.BTN_FINISH.Text = "Quitter";
            this.BTN_FINISH.UseVisualStyleBackColor = true;
            this.BTN_FINISH.Click += new System.EventHandler(this.BTN_FINISH_Click);
            // 
            // WIN_SITE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(981, 341);
            this.Controls.Add(this.BTN_FINISH);
            this.Controls.Add(this.GB_CREATE_SERVICE);
            this.Controls.Add(this.GB_CONSULT_SITE);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(999, 388);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(999, 388);
            this.Name = "WIN_SITE";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Text = "Site";
            this.Load += new System.EventHandler(this.WIN_SERVICE_Load);
            this.GB_CONSULT_SITE.ResumeLayout(false);
            this.GB_CONSULT_SITE.PerformLayout();
            this.GB_CREATE_SERVICE.ResumeLayout(false);
            this.GB_CREATE_SERVICE.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Label LBL_CONSULT_CITY;
        private ComboBox CMB_SITE;
        private GroupBox GB_CONSULT_SITE;
        private TextBox INP_CONSULT_CITY;
        private Button BTN_SAVE;
        private Button BTN_DELETE;
        private GroupBox GB_CREATE_SERVICE;
        private TextBox INP_CREATE_CITY;
        private Label LBL_CREATE_CITY;
        private Button BTN_FINISH;
        private Button BTN_CREATE;
        private Label LBL_ID;
        private TextBox INP_CRETAED_AT;
        private Label LBL_CREATED_AT;
        private TextBox INP_UPDATED_AT;
        private Label LBL_UPDATED_AT;
        private TextBox INP_ID;
        private TextBox INP_CONSULT_ADRESS;
        private Label LBL_CONSULT_ADRESS;
        private TextBox INP_CONSULT_POSTCODE;
        private Label LBL_CONSULT_POSTCODE;
        private Label LBL_CONSULT_ROLE;
        private ComboBox CMB_CONSULT_ROLE;
        private TextBox INP_CREATE_POSTCODE;
        private Label LBL_CREATE_POSTCODE;
        private ComboBox CMB_CREATE_ROLE;
        private Label LBL_CREATE_ROLE;
        private TextBox INP_CREATE_ADRESSE;
        private Label LBL_CREATE_ADRESS;
    }
}
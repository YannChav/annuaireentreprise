﻿namespace AnnuaireEntreprise
{
    partial class WIN_HOME
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WIN_HOME));
            this.INP_LASTNAME = new System.Windows.Forms.TextBox();
            this.LBL_LASTNAME = new System.Windows.Forms.Label();
            this.LBL_SERVICE = new System.Windows.Forms.Label();
            this.LBL_SITE = new System.Windows.Forms.Label();
            this.CMB_SERVICE = new System.Windows.Forms.ComboBox();
            this.CMB_SITE = new System.Windows.Forms.ComboBox();
            this.LBL_FIRSTNAME = new System.Windows.Forms.Label();
            this.INP_FIRSTNAME = new System.Windows.Forms.TextBox();
            this.BTN_SEARCH = new System.Windows.Forms.Button();
            this.DGV_COLLABORATORS = new System.Windows.Forms.DataGridView();
            this.CLN_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLN_FIRSTNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLN_LASTNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLN_PHONE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLN_CELLPHONE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLN_MAIL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLN_SERVICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLN_SITE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLN_CREATEDAT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLN_UPDATEDAT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TSMI_GESTION = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_ADMIN = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_COLLABORATORS = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_COLLAB_ADD = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_SERVICE = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_SITE = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_CANCEL = new System.Windows.Forms.ToolStripMenuItem();
            this.MENU = new System.Windows.Forms.MenuStrip();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_COLLABORATORS)).BeginInit();
            this.MENU.SuspendLayout();
            this.SuspendLayout();
            // 
            // INP_LASTNAME
            // 
            this.INP_LASTNAME.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.INP_LASTNAME.Location = new System.Drawing.Point(498, 43);
            this.INP_LASTNAME.Name = "INP_LASTNAME";
            this.INP_LASTNAME.Size = new System.Drawing.Size(231, 30);
            this.INP_LASTNAME.TabIndex = 0;
            // 
            // LBL_LASTNAME
            // 
            this.LBL_LASTNAME.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LBL_LASTNAME.AutoSize = true;
            this.LBL_LASTNAME.Location = new System.Drawing.Point(435, 47);
            this.LBL_LASTNAME.Name = "LBL_LASTNAME";
            this.LBL_LASTNAME.Size = new System.Drawing.Size(57, 23);
            this.LBL_LASTNAME.TabIndex = 1;
            this.LBL_LASTNAME.Text = "Nom :\r\n";
            // 
            // LBL_SERVICE
            // 
            this.LBL_SERVICE.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LBL_SERVICE.AutoSize = true;
            this.LBL_SERVICE.Location = new System.Drawing.Point(12, 86);
            this.LBL_SERVICE.Name = "LBL_SERVICE";
            this.LBL_SERVICE.Size = new System.Drawing.Size(72, 23);
            this.LBL_SERVICE.TabIndex = 2;
            this.LBL_SERVICE.Text = "Service :";
            // 
            // LBL_SITE
            // 
            this.LBL_SITE.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LBL_SITE.AutoSize = true;
            this.LBL_SITE.Location = new System.Drawing.Point(445, 86);
            this.LBL_SITE.Name = "LBL_SITE";
            this.LBL_SITE.Size = new System.Drawing.Size(47, 23);
            this.LBL_SITE.TabIndex = 3;
            this.LBL_SITE.Text = "Site :";
            // 
            // CMB_SERVICE
            // 
            this.CMB_SERVICE.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.CMB_SERVICE.FormattingEnabled = true;
            this.CMB_SERVICE.Location = new System.Drawing.Point(97, 84);
            this.CMB_SERVICE.Name = "CMB_SERVICE";
            this.CMB_SERVICE.Size = new System.Drawing.Size(231, 31);
            this.CMB_SERVICE.TabIndex = 4;
            // 
            // CMB_SITE
            // 
            this.CMB_SITE.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.CMB_SITE.FormattingEnabled = true;
            this.CMB_SITE.Location = new System.Drawing.Point(498, 82);
            this.CMB_SITE.Name = "CMB_SITE";
            this.CMB_SITE.Size = new System.Drawing.Size(231, 31);
            this.CMB_SITE.TabIndex = 5;
            // 
            // LBL_FIRSTNAME
            // 
            this.LBL_FIRSTNAME.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LBL_FIRSTNAME.AutoSize = true;
            this.LBL_FIRSTNAME.Location = new System.Drawing.Point(12, 43);
            this.LBL_FIRSTNAME.Name = "LBL_FIRSTNAME";
            this.LBL_FIRSTNAME.Size = new System.Drawing.Size(79, 23);
            this.LBL_FIRSTNAME.TabIndex = 6;
            this.LBL_FIRSTNAME.Text = "Prénom :";
            // 
            // INP_FIRSTNAME
            // 
            this.INP_FIRSTNAME.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.INP_FIRSTNAME.Location = new System.Drawing.Point(97, 40);
            this.INP_FIRSTNAME.Name = "INP_FIRSTNAME";
            this.INP_FIRSTNAME.Size = new System.Drawing.Size(231, 30);
            this.INP_FIRSTNAME.TabIndex = 7;
            // 
            // BTN_SEARCH
            // 
            this.BTN_SEARCH.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BTN_SEARCH.Location = new System.Drawing.Point(800, 83);
            this.BTN_SEARCH.Name = "BTN_SEARCH";
            this.BTN_SEARCH.Size = new System.Drawing.Size(120, 29);
            this.BTN_SEARCH.TabIndex = 8;
            this.BTN_SEARCH.Text = "Rechercher";
            this.BTN_SEARCH.UseVisualStyleBackColor = true;
            this.BTN_SEARCH.Click += new System.EventHandler(this.BTN_SEARCH_Click);
            // 
            // DGV_COLLABORATORS
            // 
            this.DGV_COLLABORATORS.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGV_COLLABORATORS.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGV_COLLABORATORS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_COLLABORATORS.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CLN_ID,
            this.CLN_FIRSTNAME,
            this.CLN_LASTNAME,
            this.CLN_PHONE,
            this.CLN_CELLPHONE,
            this.CLN_MAIL,
            this.CLN_SERVICE,
            this.CLN_SITE,
            this.CLN_CREATEDAT,
            this.CLN_UPDATEDAT});
            this.DGV_COLLABORATORS.Location = new System.Drawing.Point(16, 138);
            this.DGV_COLLABORATORS.Name = "DGV_COLLABORATORS";
            this.DGV_COLLABORATORS.RowHeadersWidth = 51;
            this.DGV_COLLABORATORS.RowTemplate.Height = 29;
            this.DGV_COLLABORATORS.Size = new System.Drawing.Size(928, 330);
            this.DGV_COLLABORATORS.TabIndex = 10;
            this.DGV_COLLABORATORS.DoubleClick += new System.EventHandler(this.DGV_COLLABORATORS_DoubleClick);
            // 
            // CLN_ID
            // 
            this.CLN_ID.HeaderText = "ID";
            this.CLN_ID.MinimumWidth = 6;
            this.CLN_ID.Name = "CLN_ID";
            this.CLN_ID.ReadOnly = true;
            this.CLN_ID.Visible = false;
            // 
            // CLN_FIRSTNAME
            // 
            this.CLN_FIRSTNAME.HeaderText = "Prénom";
            this.CLN_FIRSTNAME.MinimumWidth = 6;
            this.CLN_FIRSTNAME.Name = "CLN_FIRSTNAME";
            this.CLN_FIRSTNAME.ReadOnly = true;
            // 
            // CLN_LASTNAME
            // 
            this.CLN_LASTNAME.HeaderText = "Nom";
            this.CLN_LASTNAME.MinimumWidth = 6;
            this.CLN_LASTNAME.Name = "CLN_LASTNAME";
            this.CLN_LASTNAME.ReadOnly = true;
            // 
            // CLN_PHONE
            // 
            this.CLN_PHONE.HeaderText = "Tél. Fixe";
            this.CLN_PHONE.MinimumWidth = 6;
            this.CLN_PHONE.Name = "CLN_PHONE";
            this.CLN_PHONE.ReadOnly = true;
            // 
            // CLN_CELLPHONE
            // 
            this.CLN_CELLPHONE.HeaderText = "Tél. Portable";
            this.CLN_CELLPHONE.MinimumWidth = 6;
            this.CLN_CELLPHONE.Name = "CLN_CELLPHONE";
            this.CLN_CELLPHONE.ReadOnly = true;
            // 
            // CLN_MAIL
            // 
            this.CLN_MAIL.HeaderText = "Email";
            this.CLN_MAIL.MinimumWidth = 6;
            this.CLN_MAIL.Name = "CLN_MAIL";
            this.CLN_MAIL.ReadOnly = true;
            // 
            // CLN_SERVICE
            // 
            this.CLN_SERVICE.HeaderText = "Service";
            this.CLN_SERVICE.MinimumWidth = 6;
            this.CLN_SERVICE.Name = "CLN_SERVICE";
            this.CLN_SERVICE.ReadOnly = true;
            // 
            // CLN_SITE
            // 
            this.CLN_SITE.HeaderText = "Site";
            this.CLN_SITE.MinimumWidth = 6;
            this.CLN_SITE.Name = "CLN_SITE";
            this.CLN_SITE.ReadOnly = true;
            // 
            // CLN_CREATEDAT
            // 
            this.CLN_CREATEDAT.HeaderText = "Créé le";
            this.CLN_CREATEDAT.MinimumWidth = 6;
            this.CLN_CREATEDAT.Name = "CLN_CREATEDAT";
            this.CLN_CREATEDAT.ReadOnly = true;
            this.CLN_CREATEDAT.Visible = false;
            // 
            // CLN_UPDATEDAT
            // 
            this.CLN_UPDATEDAT.HeaderText = "Mis à jour le";
            this.CLN_UPDATEDAT.MinimumWidth = 6;
            this.CLN_UPDATEDAT.Name = "CLN_UPDATEDAT";
            this.CLN_UPDATEDAT.ReadOnly = true;
            this.CLN_UPDATEDAT.Visible = false;
            // 
            // TSMI_GESTION
            // 
            this.TSMI_GESTION.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMI_ADMIN,
            this.TSMI_COLLABORATORS,
            this.TSMI_SERVICE,
            this.TSMI_SITE,
            this.TSMI_CANCEL});
            this.TSMI_GESTION.Name = "TSMI_GESTION";
            this.TSMI_GESTION.Size = new System.Drawing.Size(73, 24);
            this.TSMI_GESTION.Text = "Gestion";
            // 
            // TSMI_ADMIN
            // 
            this.TSMI_ADMIN.Name = "TSMI_ADMIN";
            this.TSMI_ADMIN.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.C)));
            this.TSMI_ADMIN.Size = new System.Drawing.Size(290, 26);
            this.TSMI_ADMIN.Text = "Connexion Admin";
            this.TSMI_ADMIN.Visible = false;
            this.TSMI_ADMIN.Click += new System.EventHandler(this.TSMI_ADMIN_Click);
            // 
            // TSMI_COLLABORATORS
            // 
            this.TSMI_COLLABORATORS.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMI_COLLAB_ADD});
            this.TSMI_COLLABORATORS.Name = "TSMI_COLLABORATORS";
            this.TSMI_COLLABORATORS.Size = new System.Drawing.Size(290, 26);
            this.TSMI_COLLABORATORS.Text = "Collaborateurs";
            // 
            // TSMI_COLLAB_ADD
            // 
            this.TSMI_COLLAB_ADD.Name = "TSMI_COLLAB_ADD";
            this.TSMI_COLLAB_ADD.Size = new System.Drawing.Size(141, 26);
            this.TSMI_COLLAB_ADD.Text = "Ajouter";
            this.TSMI_COLLAB_ADD.Click += new System.EventHandler(this.TSMI_COLLAB_ADD_Click);
            // 
            // TSMI_SERVICE
            // 
            this.TSMI_SERVICE.Name = "TSMI_SERVICE";
            this.TSMI_SERVICE.Size = new System.Drawing.Size(290, 26);
            this.TSMI_SERVICE.Text = "Service";
            this.TSMI_SERVICE.Click += new System.EventHandler(this.TSMI_SERVICE_Click);
            // 
            // TSMI_SITE
            // 
            this.TSMI_SITE.Name = "TSMI_SITE";
            this.TSMI_SITE.Size = new System.Drawing.Size(290, 26);
            this.TSMI_SITE.Text = "Site";
            this.TSMI_SITE.Click += new System.EventHandler(this.TSMI_SITE_Click);
            // 
            // TSMI_CANCEL
            // 
            this.TSMI_CANCEL.Name = "TSMI_CANCEL";
            this.TSMI_CANCEL.Size = new System.Drawing.Size(290, 26);
            this.TSMI_CANCEL.Text = "Quitter";
            this.TSMI_CANCEL.Click += new System.EventHandler(this.TSMI_CANCEL_Click);
            // 
            // MENU
            // 
            this.MENU.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.MENU.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMI_GESTION});
            this.MENU.Location = new System.Drawing.Point(0, 0);
            this.MENU.Name = "MENU";
            this.MENU.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.MENU.Size = new System.Drawing.Size(952, 28);
            this.MENU.TabIndex = 11;
            this.MENU.Text = "menuStrip1";
            // 
            // WIN_HOME
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(952, 480);
            this.Controls.Add(this.DGV_COLLABORATORS);
            this.Controls.Add(this.BTN_SEARCH);
            this.Controls.Add(this.INP_FIRSTNAME);
            this.Controls.Add(this.LBL_FIRSTNAME);
            this.Controls.Add(this.CMB_SITE);
            this.Controls.Add(this.CMB_SERVICE);
            this.Controls.Add(this.LBL_SITE);
            this.Controls.Add(this.LBL_SERVICE);
            this.Controls.Add(this.LBL_LASTNAME);
            this.Controls.Add(this.INP_LASTNAME);
            this.Controls.Add(this.MENU);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MENU;
            this.MinimumSize = new System.Drawing.Size(970, 527);
            this.Name = "WIN_HOME";
            this.Text = "Annuaire Entreprise";
            this.Load += new System.EventHandler(this.Home_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_COLLABORATORS)).EndInit();
            this.MENU.ResumeLayout(false);
            this.MENU.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TextBox INP_LASTNAME;
        private Label LBL_LASTNAME;
        private Label LBL_SERVICE;
        private Label LBL_SITE;
        private ComboBox CMB_SERVICE;
        private ComboBox CMB_SITE;
        private Label LBL_FIRSTNAME;
        private TextBox INP_FIRSTNAME;
        private Button BTN_SEARCH;
        private DataGridView DGV_COLLABORATORS;
        private DataGridViewTextBoxColumn CLN_ID;
        private DataGridViewTextBoxColumn CLN_FIRSTNAME;
        private DataGridViewTextBoxColumn CLN_LASTNAME;
        private DataGridViewTextBoxColumn CLN_PHONE;
        private DataGridViewTextBoxColumn CLN_CELLPHONE;
        private DataGridViewTextBoxColumn CLN_MAIL;
        private DataGridViewTextBoxColumn CLN_SERVICE;
        private DataGridViewTextBoxColumn CLN_SITE;
        private DataGridViewTextBoxColumn CLN_CREATEDAT;
        private DataGridViewTextBoxColumn CLN_UPDATEDAT;
        private ToolStripMenuItem TSMI_GESTION;
        private ToolStripMenuItem TSMI_COLLABORATORS;
        private ToolStripMenuItem TSMI_COLLAB_ADD;
        private ToolStripMenuItem TSMI_SERVICE;
        private ToolStripMenuItem TSMI_SITE;
        private ToolStripMenuItem TSMI_CANCEL;
        private MenuStrip MENU;
        private ToolStripMenuItem TSMI_ADMIN;
    }
}
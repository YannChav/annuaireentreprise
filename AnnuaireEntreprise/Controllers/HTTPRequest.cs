﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Http;
using System.Reflection.Metadata;
using System.Text.Json;
using System.Collections;
using System.Net.Http.Json;


/*
    POUR LES PARAMETRES JSON DES METHODES POST, PUT, PATCH
            var content = new { 
                param = value 
            };
*/


namespace AnnuaireEntreprise.Controllers
{
    internal class HTTPRequest
    {
        // Propriétés
        private string uri;
        public string Uri
        {
            get { return uri; }
            set { uri = value; }
        }

        private string dataResponse;
        public string DataResponse
        {
            get { return dataResponse; }
            set { dataResponse = value; }
        }

        private bool state;
        public bool State
        {
            get { return state; }
            set { state = value; }
        }


        // Constructeur
        public HTTPRequest(string Uri)
        {
            uri = "https://localhost:7243/api/" + Uri;
        }

        // Methodes
        public void ExecuteQueryGET()
        {
            // Créé le client pour exécuter les requêtes
            HttpClient client = new HttpClient();

            // Exécute la requête et attend le retour
            using (HttpResponseMessage response = client.GetAsync(Uri).Result)
            {
                ReadResultRequest(response);
            }
        }
        public void ExecuteQueryPOST(string requestContent)
        {
            // Créé le client pour exécuter les requêtes
            HttpClient client = new HttpClient();

            // Met en place le JSON contenant les données à envoyer à l'API
            using StringContent jsonContent = new(
                requestContent,
                Encoding.UTF8,
                "application/json");

            // Exécute la requête et attend le retour
            using (HttpResponseMessage response = client.PostAsync(uri, jsonContent).Result)
            {
                ReadResultRequest(response);
            }
        }
        public void ExecuteQueryPUT(string requestContent)
        {
            // Créé le client pour exécuter les requêtes
            HttpClient client = new HttpClient();

            // Met en place le JSON contenant les données à envoyer à l'API
            using StringContent jsonContent = new(
                requestContent,
                Encoding.UTF8,
                "application/json");

            // Exécute la requête et attend le retour
            using (HttpResponseMessage response = client.PutAsync(Uri, jsonContent).Result)
            {
                ReadResultRequest(response);
            }
        }

        public void ExecuteQueryDELETE()
        {
            // Créé le client pour exécuter les requêtes
            HttpClient client = new HttpClient();

            // Exécute la requête et attend le retour
            using (HttpResponseMessage response = client.DeleteAsync(Uri).Result)
            {
                ReadResultRequest(response);
            }
        }

        private void ReadResultRequest(HttpResponseMessage responseMessage)
        {
            // Vérifie que la requête a bien été exécutée
            if (responseMessage.IsSuccessStatusCode)
            {
                // Récupère le contenu de la requête
                using (HttpContent content = responseMessage.Content)
                {
                    // Renseigne les propriétés de l'objet
                    dataResponse = content.ReadAsStringAsync().Result;
                    state = true;
                }
            }
            else
            {
                // Renseigne les propriétés de l'objet
                dataResponse = "";
                state = false;
            }
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenererDonnees
{
    internal class Service
    {
		// Propriétés
		private string name;
		private string createdAt;
		private string updatedAt;

		// Constructeur
		public Service(string Name, string CreatedAt, string UpdatedAt)
		{
			this.Name = Name;
			this.CreatedAt = CreatedAt;
			this.UpdatedAt = UpdatedAt;
		}

		// Accesseurs
		public string UpdatedAt
		{
			get { return updatedAt; }
			set { updatedAt = value; }
		}


		public string CreatedAt
		{
			get { return createdAt; }
			set { createdAt = value; }
		}


		public string Name
		{
			get { return name; }
			set { name = value; }
		}

        /*--------------------------------------------------------
         * Fonction    : Supprimer
         * Description : Vide l'ensemble de la table Service
         * --------------------------------------------------------*/
        public static void Supprimer()
        {
            // Met en place la requête à exécuter
            SqlCommand requete = new SqlCommand("DELETE FROM service",
                new SqlConnection(Connexion.ChaineDeConnexion));

            // Appelle la procédure pour exécuter la requête
            (bool result, DataSet donnees) resultat = Connexion.ExecuteRequete(requete);

            // Vérifie que la requête s'est bien exécutée
            if (resultat.result)
            {
                // Modifie les couleurs de la console
                Console.ForegroundColor = ConsoleColor.DarkGreen;

                // Affiche un message à l'utilisateur
                Console.WriteLine("Suppression des données des services réussie.");
            }
            else
            {
                // Modifie les couleurs de la console
                Console.ForegroundColor = ConsoleColor.DarkRed;

                // Affiche un message à l'utilisateur
                Console.WriteLine("Suppression des données des services échouée.");
            }

            // Réinitialise les couleurs de la console
            Console.ResetColor();
        }

        /*--------------------------------------------------------
		 * Fonction    : Generer
		 * Description : Génère une ligne d'enregistrement dans la BDD
		 * --------------------------------------------------------*/
        public void Generer()
        {
            // Met en place la requête à exécuter
            SqlCommand requete = new SqlCommand("INSERT INTO service (name, created_at, updated_at) VALUES " +
                "(@name, @createdAt, @updatedAt)", new SqlConnection(Connexion.ChaineDeConnexion));

            // Configure les paramètres pour la requête
            SqlParameter paramName = new SqlParameter("@name", name);
            paramName.Direction = ParameterDirection.Input;
            paramName.DbType = DbType.String;
            paramName.Size = 25;

            SqlParameter paramCreatedAt = new SqlParameter("@createdAt", createdAt);
            paramCreatedAt.Direction = ParameterDirection.Input;
            paramCreatedAt.DbType = DbType.Date;

            SqlParameter paramUpdatedAt = new SqlParameter("@updatedAt", updatedAt);
            paramUpdatedAt.Direction = ParameterDirection.Input;
            paramUpdatedAt.DbType = DbType.Date;

            // Envoie des parmaètres dans la requête 
            requete.Parameters.Add(paramName);
            requete.Parameters.Add(paramCreatedAt);
            requete.Parameters.Add(paramUpdatedAt);

            // Appelle la procédure pour exécuter la requête
            (bool result, DataSet donnees) resultat = Connexion.ExecuteRequete(requete);

            // Vérifie que la requête s'est bien exécutée
            if (resultat.result)
            {
                // Modifie les couleurs de la console
                Console.ForegroundColor = ConsoleColor.DarkGreen;

                // Affiche un message à l'utilisateur
                Console.WriteLine($"Génération des données du service {name} réussie.");
            }
            else
            {
                // Modifie les couleurs de la console
                Console.ForegroundColor = ConsoleColor.DarkRed;

                // Affiche un message à l'utilisateur
                Console.WriteLine($"Génération des données du service {name} échouée.");
            }

            // Réinitialise les couleurs de la console
            Console.ResetColor();
        }

        /*--------------------------------------------------------
         * Fonction    : RecupererIndice
         * Description : Récupère l'ensemble des indices disponibles 
         *      dans la base de données
         * --------------------------------------------------------*/
        public static ArrayList RecupererIndice()
        {
            // Crée un tableau vide pour le retour de la fonction
            ArrayList tabIndicesService = new ArrayList();

            // Met en place la requête à exécuter
            SqlCommand requete = new SqlCommand("SELECT id FROM service",
                new SqlConnection(Connexion.ChaineDeConnexion));

            // Appelle la procédure pour exécuter la requête
            (bool result, DataSet donnees) resultat = Connexion.ExecuteRequete(requete);

            // Vérifie que la requête s'est bien exécutée
            if (resultat.result)
            {
                // Récupère la table avec les réponses
                DataTable tableResultat = resultat.donnees.Tables[0];
                foreach (DataRow ligneResultat in tableResultat.Rows)
                {
                    // Ajout l'indice au tableau
                    tabIndicesService.Add(ligneResultat.ItemArray[0]);
                }
            }

            // Renvoie le tableau de données
            return tabIndicesService;
        }
    }
}

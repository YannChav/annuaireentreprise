﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Http;
using System.Reflection.Metadata;
using System.Text.Json;

namespace GenererDonnees
{
    internal class HTTPRequest
    {
        // Propriétés
        private string uri;
        public string Uri
        {
            get { return uri; }
            set { uri = value; }
        }

        private string dataResponse;
        public string DataResponse
        {
            get { return dataResponse; }
            set { dataResponse = value; }
        }

        private bool state;
        public bool State
        {
            get { return state; }
            set { state = value; }
        }


        // Constructeur
        public HTTPRequest(string Uri)
        {
            uri = Uri;
        }

        // Methodes
        public void ExecuteQuery()
        {
            // Créé le client pour exécuter les requêtes
            HttpClient client = new HttpClient();

            // Exécute la requête et attend le retour
            using (HttpResponseMessage response = client.GetAsync(Uri).Result)
            {
                // Vérifie que la requête a bien été exécutée
                if (response.IsSuccessStatusCode)
                {
                    // Récupère le contenu de la requête
                    using (HttpContent content = response.Content)
                    {
                        // Renseigne les propriétés de l'objet
                        dataResponse = content.ReadAsStringAsync().Result;
                        state = true;
                    }
                }
                else
                { 
                    // Renseigne les propriétés de l'objet
                    dataResponse = "";
                    state = true;
                }
            }
        }

        public (bool existence, string dataBranch) GetBranchJson(string branchString)
        {
            string branchName;
            string dataBranch = "";

            // Récupère tous les fils dans un tableau
            Array branchs = branchString.Split(".");

            // Supprimer les crochets pour convertir la chaine en JSON
            string stringJson = DataResponse.Replace("[", "");
            stringJson = stringJson.Replace("]", "");

            // Convertit la chaine en json
            using JsonDocument docJson = JsonDocument.Parse(stringJson);
            JsonElement dataJson = docJson.RootElement;

            // Récupère le nom de la première branche
            branchName = branchs.GetValue(0).ToString();

            // Parcourt toutes les premières branches
            for (int i = 1; i < branchs.Length; i++)
            {
                // Vérifie l'existence de la branche
                if (!dataJson.TryGetProperty(branchName, out dataJson))
                {
                    // Renvoie faux et une valeur nulle pour la branche
                    return (false, "");
                }

                // Récupère la branche suivante
                branchName = branchs.GetValue(i).ToString();
            }
            // Vérifie l'existence de la dernière branche
            if (!dataJson.TryGetProperty(branchName, out dataJson))
            {
                // Renvoie faux et une valeur nulle pour la branche
                return (false, "");
            }
            else
            {
                // Renvoie vrai et la valuer de la branche
                return (true, dataJson.ToString());
            }
        }
    }
}

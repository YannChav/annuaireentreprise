﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenererDonnees
{
    internal class Site : Connexion
    {
        // Propriétés
        private string city;
        private string adress;
        private string postcode;
        private string role;
        private string createdAt;
        private string updatedAt;

        // Constructeur
        public Site(string City, string Adress, string Postcode, string Role, string CreatedAt, string UpdatedAt)
        {
            city = City;
            adress = Adress;
            postcode = Postcode;
            role = Role;
            createdAt = CreatedAt;
            updatedAt = UpdatedAt;
        }

        // Accesseur
        public string UpdatedAt
        {
            get { return updatedAt; }
            set { updatedAt = value; }
        }


        public string Created_At
        {
            get { return createdAt; }
            set { createdAt = value; }
        }


        public string Role
        {
            get { return role; }
            set { role = value; }
        }


        public string Postcode
        {
            get { return postcode; }
            set { postcode = value; }
        }


        public string Adress
        {
            get { return adress; }
            set { adress = value; }
        }


        public string City
        {
            get { return city; }
            set { city = value; }
        }


        // Méthodes

        /*--------------------------------------------------------
		 * Fonction    : Supprimer
		 * Description : Vide l'ensemble de la table Site
		 * --------------------------------------------------------*/
        public static void Supprimer()
		{
            // Met en place la requête à exécuter
            SqlCommand requete = new SqlCommand("DELETE FROM site",
                new SqlConnection(Connexion.ChaineDeConnexion));

            // Appelle la procédure pour exécuter la requête
            (bool result, DataSet donnees) resultat = Connexion.ExecuteRequete(requete);

			// Vérifie que la requête s'est bien exécutée
			if (resultat.result)
			{
				// Modifie les couleurs de la console
				Console.ForegroundColor = ConsoleColor.DarkGreen;

				// Affiche un message à l'utilisateur
				Console.WriteLine("Suppression des données des sites réussie.");
            }
			else
			{ 
                // Modifie les couleurs de la console
                Console.ForegroundColor = ConsoleColor.DarkRed;

                // Affiche un message à l'utilisateur
                Console.WriteLine("Suppression des données des sites échouée.");
            }

            // Réinitialise les couleurs de la console
            Console.ResetColor();
        }

        /*--------------------------------------------------------
		 * Fonction    : Generer
		 * Description : Génère une ligne d'enregistrement dans la BDD
		 * --------------------------------------------------------*/
        public void Generer()
        {
            // Met en place la requête à exécuter
            SqlCommand requete = new SqlCommand("INSERT INTO site (city, adress, postcode, role, created_at, updated_at) VALUES " +
                "(@city, @adress, @postcode, @role, @createdAt, @updatedAt)", new SqlConnection(Connexion.ChaineDeConnexion));

            // Configure les paramètres pour la requête
            SqlParameter paramCity = new SqlParameter("@city", city);
            paramCity.Direction = ParameterDirection.Input;
            paramCity.DbType = DbType.String;
            paramCity.Size = 25;

            SqlParameter paramAdress = new SqlParameter("@adress", adress);
            paramAdress.Direction = ParameterDirection.Input;
            paramAdress.DbType = DbType.String;
            paramAdress.Size = 50;

            SqlParameter paraPostcode = new SqlParameter("@postcode", postcode);
            paraPostcode.Direction = ParameterDirection.Input;
            paraPostcode.DbType = DbType.String;
            paraPostcode.Size = 5;

            SqlParameter paramRole = new SqlParameter("@role", role);
            paramRole.Direction = ParameterDirection.Input;
            paramRole.DbType = DbType.String;
            paramRole.Size = 255;

            SqlParameter paramCreatedAt = new SqlParameter("@createdAt", createdAt);
            paramCreatedAt.Direction = ParameterDirection.Input;
            paramCreatedAt.DbType = DbType.Date;

            SqlParameter paramUpdatedAt = new SqlParameter("@updatedAt", updatedAt);
            paramUpdatedAt.Direction = ParameterDirection.Input;
            paramUpdatedAt.DbType = DbType.Date;

            // Envoie des parmaètres dans la requête 
            requete.Parameters.Add(paramCity);
            requete.Parameters.Add(paramAdress);
            requete.Parameters.Add(paraPostcode);
            requete.Parameters.Add(paramRole);
            requete.Parameters.Add(paramCreatedAt);
            requete.Parameters.Add(paramUpdatedAt);

            // Appelle la procédure pour exécuter la requête
            (bool result, DataSet donnees) resultat = Connexion.ExecuteRequete(requete);

            // Vérifie que la requête s'est bien exécutée
            if (resultat.result)
            {
                // Modifie les couleurs de la console
                Console.ForegroundColor = ConsoleColor.DarkGreen;

                // Affiche un message à l'utilisateur
                Console.WriteLine($"Génération des données du site {city} réussie.");
            }
            else
            {
                // Modifie les couleurs de la console
                Console.ForegroundColor = ConsoleColor.DarkRed;

                // Affiche un message à l'utilisateur
                Console.WriteLine($"Génération des données du site {city} échouée.");
            }

            // Réinitialise les couleurs de la console
            Console.ResetColor();
        }

        /*--------------------------------------------------------
         * Fonction    : RecupererIndice
         * Description : Récupère l'ensemble des indices disponibles 
         *      dans la base de données
         * --------------------------------------------------------*/
        public static ArrayList RecupererIndice()
        {
            // Crée un tableau vide pour le retour de la focntion
            ArrayList tabIndicesSite = new ArrayList();

            // Met en place la requête à exécuter
            SqlCommand requete = new SqlCommand("SELECT id FROM site",
                new SqlConnection(Connexion.ChaineDeConnexion));

            // Appelle la procédure pour exécuter la requête
            (bool result, DataSet donnees) resultat = Connexion.ExecuteRequete(requete);

            // Vérifie que la requête s'est bien exécutée
            if (resultat.result)
            {
                // Récupère la table avec les réponses
                DataTable tableResultat = resultat.donnees.Tables[0];
                foreach (DataRow ligneResultat in tableResultat.Rows)
                {
                    // Ajout l'indice au tableau
                    tabIndicesSite.Add(ligneResultat.ItemArray[0]);
                }
            }

            // Renvoie le tableau de données
            return tabIndicesSite;
        }
    }
}

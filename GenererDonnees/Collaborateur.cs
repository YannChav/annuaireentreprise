﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GenererDonnees
{
    internal class Collaborateur
    {
		// Propriétés et accesseurs
		private string lastname;
		public string Lastname
        {
			get { return lastname; }
			set { lastname = value; }
		}

		private string firstname;
		public string Firstname
		{
			get { return firstname; }
			set { firstname = value; }
		}

		private string phone;
		public string Phone
		{
			get { return phone; }
			set { phone = value; }
		}

		private string cellphone;
		public string Cellphone
		{
			get { return cellphone; }
			set { cellphone = value; }
		}

		private string mail;
		public string Mail
		{
			get { return mail; }
			set { mail = value; }
		}

		private int site;
		public int Site
		{
			get { return site; }
			set { site = value; }
		}

		private int service;
		public int Service
		{
			get { return service; }
			set { service = value; }
		}

		private string createdAt;
		public string CreatedAt
		{
			get { return createdAt; }
			set { createdAt = value; }
		}

		private string updatedAt;
		public string UpdatedAt
		{
			get { return updatedAt; }
			set { updatedAt = value; }
		}

        // Constructeur
        public Collaborateur(string Firstname, string Lastname, string Phone, string Cellphone,
            string Email, int Site, int Service, string CreatedAt, string UdatedAt)
        {
            firstname = Firstname;
            lastname = Lastname;
            phone = Phone;
            cellphone = Cellphone;
            mail = Email;
            service = Service;
            site = Site;
            createdAt = CreatedAt;
            updatedAt = UdatedAt;
        }

        // Méthodes 
        /*--------------------------------------------------------
         * Fonction    : Supprimer
         * Description : Vide l'ensemble de la table Collabrorateurs
         * --------------------------------------------------------*/
        public static void Supprimer()
        {
            // Met en place la requête à exécuter
            SqlCommand requete = new SqlCommand("DELETE FROM collaborator",
                new SqlConnection(Connexion.ChaineDeConnexion));

            // Appelle la procédure pour exécuter la requête
            (bool result, DataSet donnees) resultat = Connexion.ExecuteRequete(requete);

            // Vérifie que la requête s'est bien exécutée
            if (resultat.result)
            {
                // Modifie les couleurs de la console
                Console.ForegroundColor = ConsoleColor.DarkGreen;

                // Affiche un message à l'utilisateur
                Console.WriteLine("Suppression des données des collaborateurs réussie.");
            }
            else
            {
                // Modifie les couleurs de la console
                Console.ForegroundColor = ConsoleColor.DarkRed;

                // Affiche un message à l'utilisateur
                Console.WriteLine("Suppression des données des collaborateurs échouée.");
            }

            // Réinitialise les couleurs de la console
            Console.ResetColor();
        }

        /*--------------------------------------------------------
		 * Fonction    : Generer
		 * Description : Génère une ligne d'enregistrement dans la BDD
		 * --------------------------------------------------------*/
        public void Generer()
        {
            // Met en place la requête à exécuter
            SqlCommand requete = new SqlCommand("INSERT INTO collaborator " +
                "(firstname, lastname, phone, cellphone, mail, service, site, created_at, updated_at) VALUES " +
                "(@firstname, @lastname, @phone, @cellphone, @mail, @service, @site, @createdAt, @updatedAt)", 
                new SqlConnection(Connexion.ChaineDeConnexion));

            // Configure les paramètres pour la requête
            SqlParameter paramFirstname = new SqlParameter("@firstname", firstname);
            paramFirstname.Direction = ParameterDirection.Input;
            paramFirstname.DbType = DbType.String;
            paramFirstname.Size = 25;

            SqlParameter paramLastname = new SqlParameter("@lastname", lastname);
            paramLastname.Direction = ParameterDirection.Input;
            paramLastname.DbType = DbType.String;
            paramLastname.Size = 25;

            SqlParameter paramPhone = new SqlParameter("@phone", phone);
            paramPhone.Direction = ParameterDirection.Input;
            paramPhone.DbType = DbType.String;
            paramPhone.Size = 20;

            SqlParameter paramCellphone = new SqlParameter("@cellphone", cellphone);
            paramCellphone.Direction = ParameterDirection.Input;
            paramCellphone.DbType = DbType.String;
            paramCellphone.Size = 20;

            SqlParameter paramMail = new SqlParameter("@mail", mail);
            paramMail.Direction = ParameterDirection.Input;
            paramMail.DbType = DbType.String;
            paramMail.Size = 50;

            SqlParameter paramService = new SqlParameter("@service", service);
            paramService.Direction = ParameterDirection.Input;
            paramService.DbType = DbType.Int32;

            SqlParameter paramSite = new SqlParameter("@site", site);
            paramSite.Direction = ParameterDirection.Input;
            paramSite.DbType = DbType.Int32;

            SqlParameter paramCreatedAt = new SqlParameter("@createdAt", createdAt);
            paramCreatedAt.Direction = ParameterDirection.Input;
            paramCreatedAt.DbType = DbType.Date;

            SqlParameter paramUpdatedAt = new SqlParameter("@updatedAt", updatedAt);
            paramUpdatedAt.Direction = ParameterDirection.Input;
            paramUpdatedAt.DbType = DbType.Date;

            // Envoie des parmaètres dans la requête 
            requete.Parameters.Add(paramFirstname);
            requete.Parameters.Add(paramLastname);
            requete.Parameters.Add(paramPhone);
            requete.Parameters.Add(paramCellphone);
            requete.Parameters.Add(paramMail);
            requete.Parameters.Add(paramSite);
            requete.Parameters.Add(paramService);
            requete.Parameters.Add(paramCreatedAt);
            requete.Parameters.Add(paramUpdatedAt);

            // Appelle la procédure pour exécuter la requête
            (bool result, DataSet donnees) resultat = Connexion.ExecuteRequete(requete);

            // Vérifie que la requête s'est bien exécutée
            if (resultat.result)
            {
                // Modifie les couleurs de la console
                Console.ForegroundColor = ConsoleColor.DarkGreen;

                // Affiche un message à l'utilisateur
                Console.WriteLine($"Génération des données du collaborteur {firstname} {lastname} réussie.");
            }
            else
            {
                // Modifie les couleurs de la console
                Console.ForegroundColor = ConsoleColor.DarkRed;

                // Affiche un message à l'utilisateur
                Console.WriteLine($"Génération des données du collaborateur {firstname} {lastname} échouée.");
            }

            // Réinitialise les couleurs de la console
            Console.ResetColor();
        }
    }
}

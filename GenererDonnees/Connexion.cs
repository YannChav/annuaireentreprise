﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenererDonnees
{
    internal class Connexion
    {
        // Propriétés
        private static string chaineDeConnexion = "Data Source=LAPTOP-5A362EGS\\MSSQLSERVER01;" +
            "Initial Catalog =AnnuaireEntreprise" +
            ";Integrated Security=SSPI";

        // Accesseurs
        public static string ChaineDeConnexion
        {
            get { return chaineDeConnexion; }
        }

        // Méthodes

        /*--------------------------------------------------------
		 * Fonction    : ExecuteRequete
		 * Description : Exécute une requête passée en argument
		 * Paramètres  :
		 *      + cmd : La commande à exécuter
		 * --------------------------------------------------------*/
        public static (bool result, DataSet donnees) ExecuteRequete(SqlCommand cmd)
        {
            // Teste la connexion
            try
            {
                // Création de l'adapteur pour échange bi-directionnel
                SqlDataAdapter adapteur = new();

                // Attriube la requête
                adapteur.SelectCommand = cmd;

                // Instanciation du dataset
                DataSet reponseSelect = new DataSet();

                // Remplissage du DataSet par la méthode Fill
                adapteur.Fill(reponseSelect);

                return (true, reponseSelect);
            }

            catch (Exception d)
            { 
                // Modifie les couleurs de la console
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.BackgroundColor = ConsoleColor.White;

                // Affiche l'exception et renvoie un dataSet vide
                Console.WriteLine("Erreur rencontrée : " + d.Message);

                // Réinitialise les couleurs
                Console.ResetColor();

                return (false, new DataSet());
                throw;
            }
        }
    }
}


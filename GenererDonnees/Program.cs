﻿using GenererDonnees;
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Data;
using System.Text;

using System.Text.Json;
namespace GenererDonnees;

internal class Program
{
    private static void Main(string[] args)
    {
        // Met en place les dates pour Created et Updated
        string createdAt = (new DateOnly(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day)).ToString();
        string updatedAt = (new DateOnly(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day)).ToString();

        Console.WriteLine("Suppression automatique des données");

        // Supprime tous les collaborateurs
        Collaborateur.Supprimer();
        Service.Supprimer();
        Site.Supprimer();

        Console.WriteLine("");
        Console.WriteLine("Génération automatique des sites");

        // Créer les différents sites et les enregistre
        Site paris = new Site("Paris", "Champs ELysées", "75000", "Administratif", createdAt, updatedAt);
        paris.Generer();
        Site nantes = new Site("Nantes", "Rue du Breton", "44000", "Production", createdAt, updatedAt);
        nantes.Generer();
        Site toulouse = new Site("Toulouse", "Place Rose", "31000", "Production", createdAt, updatedAt);
        toulouse.Generer();
        Site nice = new Site("Nice", "Rue de la Plage", "06000", "Production", createdAt, updatedAt);
        nice.Generer();
        Site lille = new Site("Lille", "Allée de la Pluie", "59000", "Production", createdAt, updatedAt);
        lille.Generer();

        Console.WriteLine("");
        Console.WriteLine("Génération automatique des services");

        // Créer les diffénrets services et les enregistre
        Service compta = new Service("Comptabilité", createdAt, updatedAt);
        compta.Generer();
        Service prod = new Service("Production", createdAt, updatedAt);
        prod.Generer();
        Service logi = new Service("Logistique", createdAt, updatedAt);
        logi.Generer();
        Service accueil = new Service("Accueil", createdAt, updatedAt);
        accueil.Generer();
        Service info = new Service("Informatique", createdAt, updatedAt);
        info.Generer();
        Service commercial = new Service("Commercial", createdAt, updatedAt);
        commercial.Generer();
        Service ventes = new Service("Ventes", createdAt, updatedAt);
        ventes.Generer();
        Service achats = new Service("Achats", createdAt, updatedAt);
        achats.Generer();

        Console.WriteLine("");
        Console.WriteLine("Génération automatique des collaborateurs");


        ArrayList idsService = new ArrayList();
        idsService = Service.RecupererIndice();

        ArrayList idsSite = new ArrayList();
        idsSite = Site.RecupererIndice();

        // Génère les collaborateurs
        for (int i = 0; i < 500; i++)
        {
            string uri = "https://randomuser.me/api/" +
                "?nat=FR" +
                "&inc=name, email, phone, cell" +
                "&results=1";

            HTTPRequest requetePersonne = new HTTPRequest(uri);

            // Exécute la requête passée en paramètres
            requetePersonne.ExecuteQuery();

            if (requetePersonne.State)
            {
                // R2cupère les informations d'une personne créée
                (bool existenceBranch, string dataBranch) firstname = requetePersonne.GetBranchJson("results.name.first");
                (bool existenceBranch, string dataBranch) lastname = requetePersonne.GetBranchJson("results.name.last");
                (bool existenceBranch, string dataBranch) phone = requetePersonne.GetBranchJson("results.phone");
                (bool existenceBranch, string dataBranch) cellphone = requetePersonne.GetBranchJson("results.cell");
                (bool existenceBranch, string dataBranch) mail = requetePersonne.GetBranchJson("results.email");
                // Attribue un service et un site à une personne
                int tabIdService = new Random().Next(0, idsService.Count - 1);
                int service = int.Parse(idsService[tabIdService].ToString());

                int tabIdSite= new Random().Next(0, idsSite.Count - 1);
                int site = int.Parse(idsSite[tabIdSite].ToString());

                // Créé un objet Collabroateur
                Collaborateur collaborateur = new Collaborateur(firstname.dataBranch, lastname.dataBranch, phone.dataBranch, cellphone.dataBranch, mail.dataBranch,
                    site, service, createdAt, updatedAt);

                // Enregistre cet objet en BDD
                collaborateur.Generer();
            }
            else
            {
                // Modifie les couleurs de la console
                Console.ForegroundColor = ConsoleColor.DarkRed;

                Console.WriteLine("Une erreur à eu lieu lors de l'éxecution de la requête");

                // Réinitialise les couleurs de la console
                Console.ResetColor();
            }
        }

        Console.WriteLine("");
        Console.WriteLine("Génération automatique des données terminée");

    }
}